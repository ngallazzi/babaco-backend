package shared

import com.google.gson.Gson
import com.google.gson.JsonElement

fun String.unserializePHPToJson(): String {
    val parser = SerializedPhpParser(this)
    val parsed = parser.parse()
    return Gson().toJson(parsed)
}

fun String.unserializeToMap(): Map<*, *> {
    val parser = SerializedPhpParser(this)
    return parser.parse() as Map<*, *>
}

fun String.toIntList(): List<Int> {
    val elements = this
        .replace("[", "")
        .replace("]", "")
        .split(",")
    return elements.map { it.trim().toInt() }
}

fun String.toBoolean(): Boolean {
    return equals("YES", ignoreCase = true)
}

fun JsonElement.toBoolean(): Boolean {
    return this.asString.toBoolean()
}