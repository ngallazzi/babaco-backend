package shared

import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat

fun DateTime.toISO8601(): String {
    return toString(DateTimeFormat.forPattern(ISO_8601_DATE_FORMAT))
}

fun DateTime.toISO8601WithT(): String {
    return toString(DateTimeFormat.forPattern(ISO_8601_WITH_T_DATE_FORMAT))
}

fun DateTime.toPostTitle(): String {
    return toString(DateTimeFormat.forPattern(POST_TITLE_DATE_FORMATTER))
}

fun DateTime.toPostName(): String {
    return toString(DateTimeFormat.forPattern(POST_NAME_DATE_FORMATTER))
}

private const val ISO_8601_DATE_FORMAT = "yyyy-MM-dd HH:mm:ss"
private const val ISO_8601_WITH_T_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss"
private const val POST_TITLE_DATE_FORMATTER = "M dd, YYYY @ hh:mm a"
private const val POST_NAME_DATE_FORMATTER = "MMM-dd-YYYY-hhmm-a"