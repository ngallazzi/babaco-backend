package com.babacomarket.backend.controller.delivery

import com.babacomarket.backend.model.*
import com.babacomarket.backend.view.api.Result
import org.joda.time.DateTime

interface DeliveryController {
    suspend fun getDeliveryDays(zipCode: String): Result<ArrayList<DeliveryDay>>

    suspend fun getZipCodes(): Result<ArrayList<ZipCode>>
}