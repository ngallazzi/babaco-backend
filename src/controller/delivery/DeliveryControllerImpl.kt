package com.babacomarket.backend.controller.delivery

import com.babacomarket.backend.model.DeliveryDay
import com.babacomarket.backend.model.ZipCode
import com.babacomarket.backend.repository.DeliveryRepository
import com.babacomarket.backend.view.api.Result

class DeliveryControllerImpl(private val repository: DeliveryRepository) : DeliveryController {
    override suspend fun getDeliveryDays(zipCode: String): Result<ArrayList<DeliveryDay>> {
        return repository.getDeliveryDays(zipCode)
    }

    override suspend fun getZipCodes(): Result<ArrayList<ZipCode>> {
        return repository.getZipCodes()
    }

}