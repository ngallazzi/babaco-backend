package com.babacomarket.backend.controller.account

import com.babacomarket.backend.model.Subscription
import com.babacomarket.backend.model.User
import com.babacomarket.backend.repository.SubscriptionRepository
import com.babacomarket.backend.repository.UsersRepository
import com.babacomarket.backend.view.api.Result

class AccountControllerImpl(
    private val usersRepository: UsersRepository,
    private val subscriptionRepository: SubscriptionRepository
) : AccountController {

    override suspend fun getUserById(id: Int): Result<User?> {
        return usersRepository.getUserById(id)
    }

    override suspend fun getUserByUsername(username: String): Result<User?> {
        return usersRepository.userByUsername(username)
    }

    override suspend fun getSubscriptions(userId: Int): Result<List<Subscription>> {
        return subscriptionRepository.getSubscriptions(userId.toString())
    }
}