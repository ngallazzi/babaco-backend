package com.babacomarket.backend.controller.account

import com.babacomarket.backend.model.Subscription
import com.babacomarket.backend.model.User
import com.babacomarket.backend.view.api.Result

interface AccountController {

    suspend fun getUserById(id: Int): Result<User?>

    suspend fun getUserByUsername(username: String): Result<User?>

    suspend fun getSubscriptions(userId: Int):  Result<List<Subscription>>
}