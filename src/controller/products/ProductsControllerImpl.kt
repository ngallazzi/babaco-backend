package com.babacomarket.backend.controller.products

import com.babacomarket.backend.model.Product
import com.babacomarket.backend.repository.ProductRepository
import com.babacomarket.backend.view.api.Result

class ProductsControllerImpl(private val productRepository: ProductRepository) : ProductsController {
    override fun getProducts(forceUpdate: Boolean): Result<ArrayList<Product>> {
        return productRepository.getProducts(forceUpdate)
    }
}