package com.babacomarket.backend.controller.products

import com.babacomarket.backend.model.Product
import com.babacomarket.backend.view.api.Result

interface ProductsController {
    fun getProducts(forceUpdate: Boolean = false): Result<ArrayList<Product>>
}