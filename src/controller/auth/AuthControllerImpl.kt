package com.babacomarket.backend.controller.auth

import com.auth0.jwt.JWT
import com.auth0.jwt.algorithms.Algorithm
import com.babacomarket.backend.jwtClaim
import com.babacomarket.backend.model.User
import com.babacomarket.backend.repository.UsersRepository
import com.babacomarket.backend.tokenExpiresIn
import com.babacomarket.backend.view.api.Errors
import com.babacomarket.backend.view.api.Result
import io.ktor.application.*
import io.ktor.http.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import shared.PHPass
import java.util.*

class AuthControllerImpl(
    private val repository: UsersRepository,
    private val phpPass: PHPass
) : AuthController {

    override suspend fun attemptRegistration(email: String, password: String): Result<User> =
        withContext(Dispatchers.IO) {
            when (val result = repository.userByUsername(email)) {
                is Result.Error -> {
                    if (result.statusCode == HttpStatusCode.NotFound) {
                        return@withContext repository.createUser(email, phpPass.hashPassword(password))
                    }
                    return@withContext result
                }
                is Result.Success -> {
                    return@withContext Result.Error(HttpStatusCode.Conflict, Exception(Errors.USER_ALREADY_EXISTS))
                }
            }
        }

    override suspend fun attemptLogin(
        username: String,
        password: String,
        issuer: String,
        claim: String,
        algorithm: Algorithm,
        vararg audience: String
    ) = withContext(Dispatchers.IO) {
        val result = repository.findUser(username, password)
        if (result is Result.Success) {
            val token = generateToken(result.data, issuer, claim, algorithm, *audience)
            result.data.token = token
            return@withContext Result.Success(result.data)
        } else {
            return@withContext Result.Error(HttpStatusCode.Unauthorized, Exception(Errors.INVALID_CREDENTIALS))
        }
    }

    private fun generateToken(
        user: User,
        issuer: String,
        claim: String,
        algorithm: Algorithm,
        vararg audience: String,
    ): String {
        return JWT.create()
            .withAudience(*audience)
            .withIssuer(issuer)
            .withClaim(claim, user.id)
            .withExpiresAt(Date(System.currentTimeMillis() + tokenExpiresIn))
            .sign(algorithm)
    }
}