package com.babacomarket.backend.controller.auth

import com.auth0.jwt.algorithms.Algorithm
import com.babacomarket.backend.model.User
import com.babacomarket.backend.view.api.Result

interface AuthController {
    suspend fun attemptRegistration(email: String, password: String): Result<User>

    suspend fun attemptLogin(
        username: String,
        password: String,
        issuer: String,
        claim: String,
        algorithm: Algorithm,
        vararg audience: String
    ): Result<User>
}