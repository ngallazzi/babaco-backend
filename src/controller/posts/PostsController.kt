package com.babacomarket.backend.controller.posts

import com.babacomarket.backend.model.Faq
import com.babacomarket.backend.view.api.Result

interface PostsController {
    suspend fun getFaq() : Result<ArrayList<Faq>>
}