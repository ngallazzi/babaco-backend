package com.babacomarket.backend.controller.posts

import com.babacomarket.backend.model.Faq
import com.babacomarket.backend.repository.PostsRepository
import com.babacomarket.backend.view.api.Result

class PostsControllerImpl(private val postsRepository: PostsRepository) : PostsController {
    override suspend fun getFaq(): Result<ArrayList<Faq>> {
        return postsRepository.getFaq()
    }
}