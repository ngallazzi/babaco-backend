package com.babacomarket.backend.controller.checkout

import com.babacomarket.backend.model.*
import com.babacomarket.backend.repository.*
import com.babacomarket.backend.view.api.Result
import com.babacomarket.backend.view.api.checkout.requests.CardInfo
import com.babacomarket.backend.view.api.checkout.requests.OrderInfo
import com.babacomarket.backend.view.api.checkout.requests.SubscriptionRequest
import com.stripe.model.Customer
import io.ktor.client.*
import io.ktor.client.request.*
import io.ktor.http.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import pherialize.Pherialize
import shared.toIntList
import shared.unserializeToMap

class CheckoutControllerImpl(
    private val customerRepository: CustomerRepository,
    private val subscriptionRepository: SubscriptionRepository,
    private val ordersRepository: OrdersRepository,
    private val metaController: MetaController,
    private val deliveryRepository: DeliveryRepository,
    private val productRepository: ProductRepository,
    private val userMetaRepository: UserMetaRepository

) : CheckoutController {
    private suspend fun createCustomerCard(
        user: User,
        cardInfo: CardInfo
    ): Result<Customer> = withContext(Dispatchers.IO) {
        try {
            val customer = Customer.create(
                mapOf(
                    "email" to user.email,
                    "description" to "Name: " + user.displayName + ", Username:" + user.userName
                )
            )
            return@withContext customerRepository.createCardSource(customer, cardInfo)
        } catch (e: Exception) {
            return@withContext Result.Error(HttpStatusCode.BadRequest, e)
        }
    }

    override suspend fun createSubscription(request: SubscriptionRequest, user: User): Result<Int> =
        withContext(Dispatchers.IO) {
            try {
                val cardResult = createCustomerCard(user, request.paymentInfo) as Result.Success
                val orderResult = createOrder(request.orderInfo, cardResult.data, user) as Result.Success

                when (val variationResult = productRepository.getVariation(request.orderInfo.variationId)) {
                    is Result.Success -> {
                        with(orderResult.data) {
                            val createdSubscriptionResult = subscriptionRepository.createSubscription(
                                metaController.getSubscriptionPostMeta(
                                    billing.getMetas(variationResult.data),
                                    delivery.getMetas(true),
                                    shipping.getMetas(),
                                    shippingCost,
                                    product.price!!,
                                    getNextPaymentDate(),
                                    user,
                                    paymentInfo.stripeCustomerId,
                                    paymentInfo.stripeSourceId
                                ),
                                metaController.getSubscriptionItemsMetas(this)
                                    .filter { it.label == "shipping" },
                                metaController.getSubscriptionItemsMetas(this)
                                    .filter { it.label == "product" },
                                this
                            )

                            val result = createdSubscriptionResult as Result.Success
                            userMetaRepository.updateUserMeta(
                                user.id,
                                "_wcs_subscription_ids_cache",
                                Pherialize.serialize(getSubscriptionCacheUserMeta(result.data, user))
                            )
                            return@withContext (createdSubscriptionResult)
                        }
                    }
                    is Result.Error -> Result.Error(variationResult.statusCode, variationResult.exception)
                }
            } catch (e: Exception) {
                return@withContext Result.Error(HttpStatusCode.BadRequest, e)
            }
        }

    private suspend fun getSubscriptionCacheUserMeta(createdSubscriptionId: Int, user: User): ArrayList<Int> {
        val subscriptionCacheUserMeta =
            userMetaRepository.getUserMeta(user.id, "_wcs_subscription_ids_cache") as Result.Success
        val unserialized = subscriptionCacheUserMeta.data.value.unserializeToMap()
        val values = unserialized.values.map { it as Int }
        val metaValue = ArrayList(values)

        metaValue.add(createdSubscriptionId)
        return ArrayList(metaValue.sortedByDescending { it })
    }

    private suspend fun createOrder(request: OrderInfo, customer: Customer, user: User): Result<Order> =
        withContext(Dispatchers.IO) {
            try {
                val variationResult = productRepository.getVariation(request.variationId) as Result.Success
                val variation = variationResult.data

                val orderMetas = metaController.getOrderPostMetas(request, customer, variation, user)

                val productResult = productRepository.getProduct(request.variationId) as Result.Success
                val product = productResult.data

                val orderItemsMeta = metaController.getOrderItemsMeta(
                    product.id,
                    variation.toSubscriptionName(),
                    variation.id
                )

                when (val result = ordersRepository.createOrder(
                    request.customerNote,
                    product,
                    variation,
                    user.id,
                    orderMetas,
                    orderItemsMeta
                )) {
                    is Result.Success -> {
                        val createdOrder = ordersRepository.getOrder(result.data, deliveryRepository, productRepository)
                        createdOrder
                    }
                    is Result.Error -> result
                }

            } catch (e: Exception) {
                return@withContext Result.Error(HttpStatusCode.BadRequest, e)
            }
        }
}