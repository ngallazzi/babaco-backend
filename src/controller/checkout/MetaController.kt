package com.babacomarket.backend.controller.checkout

import com.babacomarket.backend.model.*
import com.babacomarket.backend.view.api.checkout.requests.OrderInfo
import com.stripe.model.Customer
import org.joda.time.DateTime
import org.joda.time.DateTimeZone
import shared.toISO8601
import shared.toISO8601WithT

class MetaController {
    fun getSubscriptionPostMeta(
        billingMetas: ArrayList<Meta>,
        deliveryMetas: ArrayList<Meta>,
        shippingMetas: ArrayList<Meta>,
        shippingCost: Double,
        productPrice: Double,
        nextPayment: DateTime,
        user: User,
        stripeCustomerId: String,
        stripeSourceId: String

    ): List<Meta> {
        val metas = arrayListOf<Meta>()
        metas.addAll(billingMetas)
        metas.add(Meta("_cancelled_email_sent", "", ""))
        // todo implement coupons
        metas.add(Meta("_cart_discount", "", "0"))
        metas.add(Meta("_cart_discount_tax", "", "0"))

        metas.add(Meta("_contains_synced_subscription", "", "true"))
        metas.add(Meta("_created_via", "", "mobile-checkout"))
        metas.add(Meta("_customer_ip_address", "", ""))
        metas.add(Meta("_customer_user", "", user.id.toString()))
        metas.add(Meta("_customer_user_agent", "", "mobile-app"))
        metas.addAll(deliveryMetas)
        metas.add(Meta("_download_permissions_granted", "", "1"))
        metas.add(Meta("_order_currency", "", "EUR"))

        // todo check this field later
        metas.add(Meta("_order_key", "", ""))
        metas.add(Meta("_order_shipping", "", shippingCost.toString()))
        metas.add(Meta("_order_shipping_tax", "", "0"))
        metas.add(Meta("_order_tax", "", "0"))
        // this should be product price + shipping cost - coupon
        metas.add(Meta("_order_total", "", (shippingCost + productPrice).toString()))
        metas.add(Meta("_order_version", "", "4.0.1"))

        // todo need to get this information from client selected payment mode
        metas.add(Meta("_payment_method", "", "stripe"))
        metas.add(Meta("_payment_method_title", "", "Carta di credito"))
        metas.add(Meta("_prices_include_tax", "", "no"))
        metas.add(Meta("_requires_manual_renewal", "", "false"))
        metas.add(Meta("_schedule_cancelled", "", "0"))
        metas.add(Meta("_schedule_end", "", "0"))

        // todo need to calculate this fields accordingly to the first delivery date
        metas.add(Meta("_schedule_next_payment", "", nextPayment.toISO8601()))
        metas.add(Meta("_schedule_payment_retry", "", "0"))
        metas.add(Meta("_schedule_start", "", DateTime.now(DateTimeZone.UTC).toISO8601()))
        metas.add(Meta("_schedule_trial_end", "", "0"))
        metas.addAll(shippingMetas)
        metas.add(Meta("_stripe_customer_id", "", stripeCustomerId))
        metas.add(Meta("_stripe_source_id", "", stripeSourceId))

        // todo check following fields
        metas.add(Meta("_subscription_renewal_order_ids_cache", "", "a:0:{}"))
        metas.add(Meta("_subscription_resubscribe_order_ids_cache", "", "a:0:{}"))
        metas.add(Meta("_suspension_count", "", "1"))
        metas.add(Meta("_trial_period", "", ""))
        metas.add(Meta("is_vat_exempt", "", "no"))
        metas.add(Meta("mailchimp_woocommerce_campaign_id", "", ""))
        metas.add(Meta("mailchimp_woocommerce_is_subscribed", "", "0"))
        metas.add(
            Meta(
                "mailchimp_woocommerce_landing_site",
                "",
                "http://staging.babacomarket.com/wp-json/wp/v2/users/me?context=edit&_locale=user"
            )
        )
        return metas
    }

    fun getSubscriptionItemsMetas(order: Order): List<Meta> {
        val metas = arrayListOf<Meta>()
        metas.add(Meta("_method_id", "shipping", order.product.id.toString()))
        metas.add(Meta("_instance_id", "shipping", order.product.id.toString()))
        metas.add(Meta("cost", "shipping", order.shippingCost.toString()))
        metas.add(Meta("total_tax", "shipping", "0"))
        metas.add(Meta("taxes", "shipping", "a:1:{s:5:\"total\";a:0:{}}"))
        metas.add(
            Meta(
                "Prodotti",
                "shipping",
                order.product.variations!!.first() { it.id == order.variationId }.toMeta()
            )
        )

        metas.add(Meta("_product_id", "product", order.product.id.toString()))
        metas.add(Meta("_variation_id", "product", order.variationId.toString()))
        metas.add(Meta("_qty", "product", "1"))
        metas.add(Meta("_tax_class", "product", ""))
        metas.add(Meta("_line_subtotal", "product", order.product.price.toString()))
        metas.add(Meta("_line_subtotal_tax", "product", "0"))
        // todo check this when coupons are implemented
        metas.add(Meta("_line_total", "product", order.product.price.toString()))
        metas.add(Meta("_line_tax", "product", "0"))
        metas.add(Meta("_line_tax_data", "product", "a:2:{s:5:\"total\";a:0:{}s:8:\"subtotal\";a:0:{}}"))
        metas.add(Meta("tipo_abbonamento", "product", order.product.variations!!.first().toSubscriptionName()))
        metas.add(Meta("_synced_sign_up_fee", "product", "0"))

        return metas
    }

    fun getOrderPostMetas(
        request: OrderInfo,
        customer: Customer,
        variation: Product.Variation,
        user: User
    ): List<Meta> {
        val metas = arrayListOf<Meta>()
        metas.addAll(request.billing.getMetas(variation))
        metas.add(Meta("_cart_discount", "", ""))
        metas.add(Meta("_cart_discount_tax", "", "0"))
        //todo check this field
        metas.add(Meta("_cart_discount_hash", "", "0"))
        metas.add(Meta("_completed_date", "", DateTime.now().toISO8601WithT()))
        metas.add(Meta("_created_via", "", "mobile-checkout"))
        //todo check this field
        metas.add(Meta("_customer_ip_address", "", ""))
        metas.add(Meta("_customer_user", "", user.id.toString()))
        //todo check this field
        metas.add(Meta("_customer_user_agent", "", ""))
        //todo check this field
        metas.add(Meta("_date_completed", "", ""))
        //todo check this field
        metas.add(Meta("_date_paid", "", ""))
        metas.addAll(request.delivery.getMetas(false))
        metas.add(Meta("_download_permissions_granted", "", "yes"))
        metas.add(Meta("_order_currency", "", "EUR"))
        //todo check this field
        metas.add(Meta("_order_key", "", ""))
        metas.add(Meta("_order_shipping", "", "0.00"))
        metas.add(Meta("_order_shipping_tax", "", "0"))
        metas.add(Meta("_order_stock_reduced", "", "true"))
        metas.add(Meta("_order_tax", "", "0"))
        metas.add(Meta("_order_total", "", "0.00"))
        metas.add(Meta("_order_version", "", "4.0.1"))
        metas.add(Meta("_paid_date", "", DateTime.now().toISO8601WithT()))
        metas.add(Meta("_payment_method", "", "stripe"))
        metas.add(Meta("_payment_method_title", "", "Carta di credito"))
        metas.add(Meta("_prices_include_tax", "", "true"))
        metas.add(Meta("_recorded_coupon_usage_counts", "", "yes"))
        metas.add(Meta("_recorded_sales", "", "yes"))
        metas.addAll(request.shipping.getMetas())
        metas.add(Meta("_stripe_customer_id", "", customer.id))
        metas.add(Meta("_stripe_source_id", "", customer.defaultSource))
        metas.add(Meta("is_vat_exempt", "", "no"))
        metas.add(Meta("mailchimp_woocommerce_campaign_id", "", ""))
        metas.add(Meta("mailchimp_woocommerce_is_subscribed", "", "0"))
        metas.add(Meta("mailchimp_woocommerce_landing_site", "", ""))
        return metas
    }

    fun getOrderItemsMeta(
        productId: Int,
        subscriptionType: String,
        variationId: Int
    ): ArrayList<Meta> {
        val metas = arrayListOf<Meta>()
        // todo check these
        metas.add(Meta("_line_subtotal", "", "0"))
        metas.add(Meta("_line_subtotal_tax", "", "0"))
        metas.add(Meta("_line_tax", "", "0"))
        metas.add(Meta("_line_tax_data", "", "a:2:{s:5:\"total\";a:0:{}s:8:\"subtotal\";a:0:{}}"))
        metas.add(Meta("_line_total", "", "0"))

        metas.add(Meta("_product_id", "", productId.toString()))
        metas.add(Meta("_variation_id", "", variationId.toString()))
        metas.add(Meta("_qty", "", "1"))
        metas.add(Meta("_tax_class", "", ""))



        metas.add(Meta("_synced_sign_up_fee", "", "0"))
        metas.add(Meta("_reduced_stock", "", "0"))
        metas.add(Meta("discount_amount", "", "0"))
        metas.add(Meta("discount_amount_tax", "", "0"))
        metas.add(Meta("_line_total", "", "0"))


        metas.add(Meta("cost", "", "0"))
        metas.add(Meta("method_id", "", "0"))
        metas.add(Meta("Prodotti", "", "0"))
        metas.add(Meta("taxes", "", "0"))
        metas.add(Meta("tipo_abbonamento", "", subscriptionType))
        metas.add(Meta("total_tax", "", subscriptionType))
        return metas
    }
}