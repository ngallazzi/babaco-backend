package com.babacomarket.backend.controller.checkout

import com.babacomarket.backend.model.Subscription
import com.babacomarket.backend.model.User
import com.babacomarket.backend.view.api.Result
import com.babacomarket.backend.view.api.checkout.requests.SubscriptionRequest

interface CheckoutController {
    suspend fun createSubscription(request : SubscriptionRequest, user: User): Result<Int>
}