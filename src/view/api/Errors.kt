package com.babacomarket.backend.view.api

class Errors{
    companion object{
        const val UNABLE_TO_CREATE_SUBSCRIPTION = "Unable to create the subscription"
        const val PRODUCT_NOT_FOUND = "Product not found"
        const val INVALID_CREDENTIALS = "Invalid credentials"
        const val REGISTRATION_ERROR = "An error has occurred during registration"
        const val USER_ALREADY_EXISTS = "User already exists"
        const val MANDATORY_USERNAME = "Username is mandatory"
        const val USER_NOT_FOUND = "User not found"
        const val NO_SUBSCRIPTION_FOUND = "No subscriptions found for the user"
        const val DELIVERY_TIME_FRAME_NOT_FOUND = "Delivery time frame not found"
    }
}