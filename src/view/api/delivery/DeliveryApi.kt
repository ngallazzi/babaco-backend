package com.babacomarket.backend.view.api.delivery

import com.babacomarket.backend.API_VERSION
import com.babacomarket.backend.controller.delivery.DeliveryController
import com.babacomarket.backend.view.api.DataResponse
import com.babacomarket.backend.view.api.ErrorResponse
import com.babacomarket.backend.view.api.Result
import io.ktor.application.*
import io.ktor.http.*
import io.ktor.response.*
import io.ktor.routing.*

const val DELIVERY_ENDPOINT = "$API_VERSION/delivery"
const val DELIVERY_DAYS_WITH_ZIPCODE_ENDPOINT = "$API_VERSION/delivery/days/{zipCode}"
const val ZIP_CODES_ENDPOINT = "$DELIVERY_ENDPOINT/zipcodes"

fun Routing.delivery(controller: DeliveryController) {
    get(DELIVERY_DAYS_WITH_ZIPCODE_ENDPOINT) {
        val zipCode = call.parameters["zipCode"]
        when (val result = controller.getDeliveryDays(zipCode!!)) {
            is Result.Success -> call.respond(DataResponse(result.data.toArray()))
            is Result.Error -> call.respond(
                HttpStatusCode.InternalServerError,
                ErrorResponse(result.statusCode.value, result.exception?.message)
            )
        }
    }

    get(ZIP_CODES_ENDPOINT) {
        when (val result = controller.getZipCodes()) {
            is Result.Success -> call.respond(DataResponse(result.data.toArray()))
            is Result.Error -> call.respond(
                HttpStatusCode.InternalServerError,
                ErrorResponse(result.statusCode.value, result.exception?.message)
            )
        }
    }
}