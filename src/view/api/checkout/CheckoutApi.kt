package com.babacomarket.backend.view.api.checkout

import com.babacomarket.backend.API_VERSION
import com.babacomarket.backend.AUTH_CONFIG_NAME
import com.babacomarket.backend.controller.checkout.CheckoutController
import com.babacomarket.backend.user
import com.babacomarket.backend.view.api.DataResponse
import com.babacomarket.backend.view.api.ErrorResponse
import com.babacomarket.backend.view.api.Result
import com.babacomarket.backend.view.api.checkout.requests.SubscriptionRequest
import com.stripe.Stripe
import io.ktor.application.*
import io.ktor.auth.*
import io.ktor.http.*
import io.ktor.request.*
import io.ktor.response.*
import io.ktor.routing.*


const val CHECKOUT_API_ENDPOINT = "$API_VERSION/checkout"
const val SUBSCRIPTION_ENDPOINT = "$CHECKOUT_API_ENDPOINT/subscription"

fun Route.checkout(
    checkoutController: CheckoutController
) {
    Stripe.apiKey = System.getenv("STRIPE_KEY")

    authenticate(AUTH_CONFIG_NAME) {
        post(SUBSCRIPTION_ENDPOINT) {
            val request = call.receive<SubscriptionRequest>()
            when (val result = checkoutController.createSubscription(request, call.user!!)) {
                is Result.Success -> call.respond(HttpStatusCode.Created, DataResponse(result.data!!))
                is Result.Error -> call.respond(
                    result.statusCode,
                    ErrorResponse(result.statusCode.value, result.exception.toString())
                )
            }
        }
    }

}