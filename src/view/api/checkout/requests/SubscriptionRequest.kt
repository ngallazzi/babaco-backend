package com.babacomarket.backend.view.api.checkout.requests

import com.google.gson.annotations.SerializedName

class SubscriptionRequest(
    @SerializedName("order_info")
    val orderInfo: OrderInfo,
    @SerializedName("payment_info")
    val paymentInfo: CardInfo
)

data class CardInfo(
    @SerializedName("number") val number: String,
    @SerializedName("expire_month") val expMonth: Int,
    @SerializedName("expire_year") val expYear: Int,
    @SerializedName("cvc") val cvc: Int,
    @SerializedName("name") val name: String
)
