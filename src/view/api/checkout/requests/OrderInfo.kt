package com.babacomarket.backend.view.api.checkout.requests

import com.babacomarket.backend.model.*
import com.babacomarket.backend.model.shipping.Shipping
import com.google.gson.annotations.SerializedName
import model.billing.Billing

class OrderInfo(
    val billing: Billing,
    val shipping: Shipping,
    @SerializedName("customer_note")
    val customerNote: String = "",
    @SerializedName("product_variation_id")
    val variationId: Int,
    @SerializedName("first_delivery")
    val delivery: Delivery
)
