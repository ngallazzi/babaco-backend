package com.babacomarket.backend.view.api.auth

import com.auth0.jwt.algorithms.Algorithm
import com.babacomarket.backend.controller.auth.AuthController
import com.babacomarket.backend.jwtClaim
import com.babacomarket.backend.view.api.DataResponse
import com.babacomarket.backend.view.api.ErrorResponse
import com.babacomarket.backend.view.api.Result
import io.ktor.application.*
import io.ktor.http.*
import io.ktor.request.*
import io.ktor.response.*
import io.ktor.routing.*

const val LOGIN_ENDPOINT = "/login"

data class Login(val username: String, val password: String)

const val REGISTRATION_ENDPOINT = "/register"

data class Registration(val email: String, val password: String)

const val LOGOUT_ENDPOINT = "/logout"

fun Route.auth(
    controller: AuthController,
    jwtIssuer: String,
    jwtClaim: String,
    algorithm: Algorithm,
    vararg jwtAudience: String
) {
    post(LOGIN_ENDPOINT) {
        val body = call.receive<Login>()
        when (val result =
            controller.attemptLogin(body.username, body.password, jwtIssuer, jwtClaim, algorithm, *jwtAudience)) {
            is Result.Success -> call.respond(DataResponse(result.data))
            is Result.Error -> call.respond(
                HttpStatusCode.InternalServerError,
                ErrorResponse(result.statusCode.value, result.exception?.message)
            )
        }

    }

    post(REGISTRATION_ENDPOINT) {
        val body = call.receive<Registration>()
        when (val result = controller.attemptRegistration(body.email, body.password)) {
            is Result.Success -> call.respond(HttpStatusCode.Created, DataResponse(result.data))
            is Result.Error -> call.respond(
                HttpStatusCode.InternalServerError,
                ErrorResponse(result.statusCode.value, result.exception?.message)
            )
        }
    }

    get(LOGOUT_ENDPOINT) {
        // todo implement this
        call.respond("Not yet implemented")
    }
}