package com.babacomarket.backend.view.api.posts

import com.babacomarket.backend.API_VERSION
import com.babacomarket.backend.controller.posts.PostsController
import com.babacomarket.backend.view.api.DataResponse
import com.babacomarket.backend.view.api.ErrorResponse
import com.babacomarket.backend.repository.PostsRepository
import com.babacomarket.backend.view.api.Result
import io.ktor.application.*
import io.ktor.http.*
import io.ktor.response.*
import io.ktor.routing.*

const val POST_API_ENDPOINT = "$API_VERSION/posts"
const val FAQ_API_ENDPOINT = "$POST_API_ENDPOINT/faq"

fun Route.posts(controller: PostsController) {
    get(POST_API_ENDPOINT) {
        try {
            // todo implement this
            call.respond("Not yet implemented")
        } catch (e: Exception) {
            call.respond(
                HttpStatusCode.InternalServerError,
                ErrorResponse(500, "")
            )
        }
    }
    get(FAQ_API_ENDPOINT) {
        when (val result = controller.getFaq()) {
            is Result.Success -> call.respond(DataResponse(result.data.toArray()))
            is Result.Error -> call.respond(result.statusCode, ErrorResponse(result.statusCode.value, result.exception?.message))
        }
    }
}