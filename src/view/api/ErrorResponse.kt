package com.babacomarket.backend.view.api

class ErrorResponse(val code: Int, val message: String?)