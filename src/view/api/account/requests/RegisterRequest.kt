package com.babacomarket.backend.application.view.api.account.requests


data class RegisterRequest(
    val username: String,
    val email: String,
    val password: String)