package com.babacomarket.backend.view.api.account

import com.babacomarket.backend.API_VERSION
import com.babacomarket.backend.AUTH_CONFIG_NAME
import com.babacomarket.backend.controller.account.AccountController
import com.babacomarket.backend.user
import com.babacomarket.backend.view.api.DataResponse
import com.babacomarket.backend.view.api.ErrorResponse
import com.babacomarket.backend.view.api.Result
import io.ktor.application.*
import io.ktor.auth.*
import io.ktor.response.*
import io.ktor.routing.*

const val ACCOUNT_ENDPOINT = "$API_VERSION/account"

const val SUBSCRIPTION_ENDPOINT = "$ACCOUNT_ENDPOINT/subscription"
const val CUSTOMER_ENDPOINT = "$ACCOUNT_ENDPOINT/customer"
const val ADDRESSES_ENDPOINT = "$CUSTOMER_ENDPOINT/addresses"

fun Routing.account(
    controller: AccountController
) {
    authenticate(AUTH_CONFIG_NAME) {
        get(CUSTOMER_ENDPOINT) {
            when (val result = controller.getUserByUsername(call.user!!.userName)) {
                is Result.Success -> call.respond(DataResponse(result.data!!))
                is Result.Error -> call.respond(
                    result.statusCode,
                    ErrorResponse(result.statusCode.value, result.exception?.message)
                )
            }
        }
    }

    authenticate(AUTH_CONFIG_NAME) {
        put(CUSTOMER_ENDPOINT) {
            // todo implement this
            call.respondText("not implemented")
        }
    }

    authenticate(AUTH_CONFIG_NAME) {
        get(ADDRESSES_ENDPOINT) {
            // todo implement this
            call.respondText("not implemented")
        }
    }

    authenticate(AUTH_CONFIG_NAME) {
        post(ADDRESSES_ENDPOINT) {
            // todo implement this
            call.respondText("not implemented")
        }
    }

    authenticate(AUTH_CONFIG_NAME) {
        put(ADDRESSES_ENDPOINT) {
            // todo implement this
            call.respondText("not implemented")
        }
    }

    authenticate(AUTH_CONFIG_NAME) {
        get(SUBSCRIPTION_ENDPOINT) {
            when (val result = controller.getSubscriptions(call.user!!.id)) {
                is Result.Success -> call.respond(DataResponse(result.data))
                is Result.Error -> call.respond(
                    result.statusCode,
                    ErrorResponse(result.statusCode.value, result.exception?.message)
                )
            }
        }
        post(SUBSCRIPTION_ENDPOINT) {
            // todo implement this
            call.respondText("not implemented")
        }
        put(SUBSCRIPTION_ENDPOINT) {
            // todo implement this
            call.respondText("not implemented")
        }
    }
}



