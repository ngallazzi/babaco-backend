package com.babacomarket.backend.view.api.products

import com.babacomarket.backend.API_VERSION
import com.babacomarket.backend.controller.products.ProductsController
import com.babacomarket.backend.view.api.DataResponse
import com.babacomarket.backend.view.api.ErrorResponse
import com.babacomarket.backend.view.api.Result
import io.ktor.application.*
import io.ktor.http.*
import io.ktor.response.*
import io.ktor.routing.*

const val PRODUCTS_API_ENDPOINT = "$API_VERSION/products"

fun Route.products(controller: ProductsController) {
    get(PRODUCTS_API_ENDPOINT) {
        when (val result = controller.getProducts(forceUpdate = true)) {
            is Result.Success -> call.respond(DataResponse(result.data.toArray()))
            is Result.Error -> call.respond(
                HttpStatusCode.InternalServerError,
                ErrorResponse(result.statusCode.value, result.exception?.message)
            )
        }
    }
}

