package com.babacomarket.backend.view.webapp

import io.ktor.application.*
import io.ktor.freemarker.*
import io.ktor.response.*
import io.ktor.routing.*

fun Routing.home() {
    get("/") {
        call.respond(FreeMarkerContent("home.ftl", null))
    }
}