package com.babacomarket.backend


import com.auth0.jwt.algorithms.Algorithm
import com.babacomarket.backend.controller.account.AccountControllerImpl
import com.babacomarket.backend.controller.auth.AuthControllerImpl
import com.babacomarket.backend.controller.checkout.CheckoutControllerImpl
import com.babacomarket.backend.controller.checkout.MetaController
import com.babacomarket.backend.controller.delivery.DeliveryControllerImpl
import com.babacomarket.backend.controller.posts.PostsControllerImpl
import com.babacomarket.backend.controller.products.ProductsControllerImpl
import com.babacomarket.backend.repository.*
import org.jetbrains.exposed.sql.Database
import shared.PHPass


private val metaController by lazy {
    MetaController()
}

private val customerRepository by lazy {
    CustomerRepositoryImpl()
}

private val deliveryRepository by lazy {
    DeliveryRepositoryImpl()
}

private val ordersRepository by lazy {
    OrdersRepositoryImpl()
}

private val postsRepository by lazy {
    PostsRepositoryImpl()
}

private val productRepository by lazy {
    ProductRepositoryImpl()
}

private val subscriptionRepository by lazy {
    SubscriptionRepositoryImpl(deliveryRepository, ordersRepository)
}

private val usersRepository by lazy {
    UsersRepositoryImpl()
}

val authController by lazy {
    AuthControllerImpl(usersRepository, PHPass(8))
}

val accountController by lazy {
    AccountControllerImpl(usersRepository, subscriptionRepository)
}

val deliveryController by lazy {
    DeliveryControllerImpl(deliveryRepository)
}

val productsController by lazy {
    ProductsControllerImpl(productRepository)
}

val postsController by lazy {
    PostsControllerImpl(postsRepository)
}

val userMetaRepository by lazy {
    UserMetaRepositoryImpl()
}

val checkoutController by lazy {
    CheckoutControllerImpl(
        customerRepository,
        subscriptionRepository,
        ordersRepository,
        metaController,
        deliveryRepository,
        productRepository,
        userMetaRepository
    )
}

const val tokenExpiresIn = 3_600_000 * 24
const val jwtIssuer = "sso.anagramma.com"
val jwtAudience = arrayOf("com.anagramma.babacomarket","ios.anagramma.babacomarket")
const val jwtClaim = "id"
const val jwtRealm = "com.babacomarket.api"
val algorithm = Algorithm.HMAC256("secret")!!

fun initDB() {
    try {
        val driver = "com.mysql.cj.jdbc.Driver"
        val db = Database.connect(
            "jdbc:mysql://35.214.232.219:3306/myfoody0_wp91?useUnicode=true&serverTimezone=UTC",
            driver,
            user = "myfoody0_nicolag",
            password = "n850812G!"
        )
        db.useNestedTransactions = true

    } catch (e: Exception) {
        println(e)
    }
}



