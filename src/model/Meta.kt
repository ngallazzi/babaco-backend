package com.babacomarket.backend.model

data class Meta(val key: String, val label: String? = null, val value: String)