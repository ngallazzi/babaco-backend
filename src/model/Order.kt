package com.babacomarket.backend.model

import com.babacomarket.backend.model.shipping.Shipping
import com.google.gson.annotations.SerializedName
import model.billing.Billing
import org.joda.time.DateTime
import java.time.DayOfWeek

data class Order(
    val id: Int,
    val date: String,
    @SerializedName("customer_note")
    val customerNote: String,
    val status: String,
    val billing: Billing,
    val delivery: Delivery,
    val product: Product,
    @SerializedName("variation_id")
    val variationId: Int,
    @SerializedName("shipping_cost")
    val shippingCost: Double,
    val total: Double,
    val shipping: Shipping,
    @SerializedName("payment_info")
    val paymentInfo: PaymentInfo
) {
    fun getNextPaymentDate(): DateTime {
        val deliveryDate = DateTime.parse(delivery.date)
        return deliveryDate.minusDays(deliveryDate.dayOfWeek).plusDays(DayOfWeek.MONDAY.value)
    }
}
