package com.babacomarket.backend.model


data class Post(val id: Int, val date: String, val title: String, val content: String)