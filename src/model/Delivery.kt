package com.babacomarket.backend.model

import com.google.gson.annotations.SerializedName
import org.joda.time.DateTime
import pherialize.Serializer


class Delivery(
    @SerializedName("date")
    val date: String,
    @SerializedName("time_frame")
    val timeFrame: TimeFrame
) {
    fun getMetas(isSubscription: Boolean): ArrayList<Meta> {
        val metas = arrayListOf<Meta>()
        metas.add(Meta("_delivery_date", "", date))
        if (isSubscription) {
            metas.add(Meta("_delivery_days", "", getDeliveryDaysMeta(date, timeFrame)))
            metas.add(Meta("_delivery_time_frame", "", timeFrame.key))
        } else {
            metas.add(Meta("_delivery_time_frame", "", getDeliveryTimeFrameMeta(timeFrame)))
        }
        return metas
    }

    private fun getDeliveryDaysMeta(date: String, timeFrame: TimeFrame): String {
        val days = ArrayList<Any?>()
        val parsedDate = DateTime.parse(date)
        for (dayIndex: Int in 0..6) {
            if (dayIndex == parsedDate.dayOfWeek().get()) {
                days.add(mapOf("enabled" to "yes", "time_frame" to timeFrame.key))
            } else {
                days.add(mapOf("enabled" to "no", "time_frame" to ""))
            }
        }
        return Serializer().serialize(days)
    }

    private fun getDeliveryTimeFrameMeta(timeFrame: TimeFrame): String {
        val map = mapOf("time_from" to timeFrame.timeFrom, "time_to" to timeFrame.timeTo)
        return Serializer().serialize(map)
    }
}

