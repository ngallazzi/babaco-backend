package com.babacomarket.backend.model


data class OrderItem(val id: Int, val name: String, val type: Type?, val orderId: Int) {
    enum class Type(val id: String) {
        SHIPPING("shipping"),
        LINE_ITEM("line_item"),
        COUPON("coupon");

        override fun toString() : String{
            return id
        }
    }
}