package com.babacomarket.backend.model

import com.babacomarket.backend.model.shipping.ShippingMethod
import com.google.gson.annotations.SerializedName

open class TimeFrame(
    @SerializedName("key")
    var key: String,
    @SerializedName("title")
    val title: String,

    @SerializedName("time_from")
    val timeFrom: String,

    @SerializedName("time_to")
    val timeTo: String,

    @SerializedName("shipping_methods")
    val shippingMethods: List<ShippingMethod>?
)