package model.billing

import com.babacomarket.backend.model.Meta
import com.babacomarket.backend.model.Product
import com.babacomarket.backend.model.shipping.Shipping
import com.google.gson.annotations.SerializedName

class Billing(
    firstName: String,
    lastName: String,
    address: String,
    city: String,
    state: String,
    postCode: String,
    @SerializedName("fiscal_code")
    val fiscalCode: String,
    country: String = "IT",
    @SerializedName("email")
    val email: String,
    @SerializedName("phone")
    val phone: String,
    val interval: Int? = null,
    val period: String? = null
) : Shipping(firstName, lastName, address, city, state, postCode, country) {
    companion object {
        fun getInstance(metas: List<Pair<String, String>>, period: String, interval: Int): Billing {
            val billingFirstName = metas.find { it.first == "_billing_first_name" }?.second!!
            val billingLastName = metas.find { it.first == "_billing_last_name" }?.second!!
            val billingAddress = metas.find { it.first == "_billing_address_1" }?.second!!
            val billingCity = metas.find { it.first == "_billing_city" }?.second!!
            val billingCountry = metas.find { it.first == "_billing_country" }?.second!!
            val billingPostCode = metas.find { it.first == "_billing_postcode" }?.second!!
            val billingState = metas.find { it.first == "_billing_state" }?.second!!
            val billingFiscalCode = metas.find { it.first == "_billing_codice_fiscale" }?.second!!
            val billingEmail = metas.find { it.first == "_billing_email" }?.second!!
            val billingPhone = metas.find { it.first == "_billing_phone" }?.second!!
            return Billing(
                billingFirstName,
                billingLastName,
                billingAddress,
                billingCity,
                billingState,
                billingPostCode,
                billingFiscalCode,
                billingCountry,
                billingEmail,
                billingPhone,
                interval,
                period
            )
        }

        fun getInstance(metas: List<Pair<String, String>>): Billing {
            val billingFirstName = metas.find { it.first == "_billing_first_name" }?.second!!
            val billingLastName = metas.find { it.first == "_billing_last_name" }?.second!!
            val billingAddress = metas.find { it.first == "_billing_address_1" }?.second!!
            val billingCity = metas.find { it.first == "_billing_city" }?.second!!
            val billingCountry = metas.find { it.first == "_billing_country" }?.second!!
            val billingPostCode = metas.find { it.first == "_billing_postcode" }?.second!!
            val billingState = metas.find { it.first == "_billing_state" }?.second!!
            val billingFiscalCode = metas.find { it.first == "_billing_codice_fiscale" }?.second!!
            val billingEmail = metas.find { it.first == "_billing_email" }?.second!!
            val billingPhone = metas.find { it.first == "_billing_phone" }?.second!!
            val billingPeriod = metas.find { it.first == "_billing_period" }?.second!!
            val billingInterval = metas.find { it.first == "_billing_interval" }?.second!!
            return Billing(
                billingFirstName,
                billingLastName,
                billingAddress,
                billingCity,
                billingState,
                billingPostCode,
                billingFiscalCode,
                billingCountry,
                billingEmail,
                billingPhone,
                billingInterval.toInt(),
                billingPeriod
            )
        }
    }

    fun getMetas(variation: Product.Variation): ArrayList<Meta> {
        val metas = arrayListOf<Meta>()
        metas.add(Meta("_billing_address_1", "", address))
        metas.add(Meta("_billing_address_2", "", ""))
        metas.add(Meta("_billing_address_index", "", getBillingAddressIndex()))
        metas.add(Meta("_billing_city", "", city))
        metas.add(Meta("_billing_codice_fiscale", "", fiscalCode))
        metas.add(Meta("_billing_company", "", ""))
        metas.add(Meta("_billing_country", "", country))
        metas.add(Meta("_billing_email", "", email))
        metas.add(Meta("_billing_email_confirmation", "", email))
        metas.add(Meta("_billing_first_name", "", firstName))
        metas.add(Meta("_billing_interval", "", variation.getBillingInterval().toString()))
        metas.add(Meta("_billing_last_name", "", lastName))
        metas.add(Meta("_billing_period", "", variation.getBillingPeriod()))
        metas.add(Meta("_billing_phone", "", phone))
        metas.add(Meta("_billing_postcode", "", postCode))
        metas.add(Meta("_billing_state", "", state))
        return metas
    }

    private fun getBillingAddressIndex(): String {
        return "$firstName $lastName $address $city $state $postCode $country $email $phone"
    }
}