package com.babacomarket.backend.model

data class ZipCode(val id: Int, val code: Int, val city: String)