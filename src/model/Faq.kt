package com.babacomarket.backend.model

data class Faq(val title: String, val description: String)