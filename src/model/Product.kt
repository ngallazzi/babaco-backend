package com.babacomarket.backend.model

import com.google.gson.annotations.SerializedName

data class Product(
    val id: Int,
    val name: String,
    @SerializedName("short_description")
    val shortDescription: String?,
    val description: String?
) {
    var images: List<Image>? = null
    var price: Double? = null
    var variations: List<Variation>? = null

    data class Image(val id: Int, val src: String)

    data class Variation(val id: Int, val name: String) {
        fun toSubscriptionName(): String {
            return this.name.split("-").last().trim()
        }

        fun toMeta(): String {
            return this.name + " &times; 1"
        }

        fun getBillingPeriod(): String {
            return "week"
        }

        fun getBillingInterval(): Int {
            if (name.contains("settimanale")) {
                return 1
            } else if (name.contains("quindicinale")) {
                return 2
            }
            return -1
        }
    }
}
