package com.babacomarket.backend.model.database

import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.IdTable
import org.jetbrains.exposed.sql.Column
import org.jetbrains.exposed.sql.jodatime.datetime

object WcOrderProductLookup : IdTable<Int>("wpv1_wc_order_product_lookup") {
    override val id: Column<EntityID<Int>> = integer("order_item_id").entityId()
    val orderId = integer("order_id")
    val productId = integer("product_id")
    val variationId = integer("variation_id")
    val customerId = integer("customer_id")
    val dateCreated = datetime("date_created")
    val productQty = integer("product_qty")
    val productNetRevenue = double("product_net_revenue")
    val productGrossRevenue = double("product_gross_revenue")
    val couponAmount = double("coupon_amount")
    val taxAmount = double("tax_amount")
    val shippingAmount = double("shipping_amount")
    val shippingTaxAmount = double("shipping_tax_amount")
}