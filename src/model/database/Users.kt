package com.babacomarket.backend.model.database

import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.IdTable
import org.jetbrains.exposed.sql.Column
import org.jetbrains.exposed.sql.jodatime.datetime

object Users : IdTable<Int>("wpv1_users") {
    override val id: Column<EntityID<Int>> = integer("id").entityId()
    val username = varchar("user_login", 60)
    val passwordHash = varchar("user_pass", 255)
    val email = varchar("user_email", 100)
    val registrationDate = datetime("user_registered")
    val displayName = varchar("display_name", 250)
}