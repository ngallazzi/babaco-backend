package com.babacomarket.backend.model.database

import org.jetbrains.exposed.sql.Column
import org.jetbrains.exposed.sql.Table

object ZipCodes : Table("wpv1_check_pincode_p") {
    val id: Column<Int> = integer("id").uniqueIndex()
    val code = integer("pincode")
    val city: Column<String> = varchar("city", 50)
}