package com.babacomarket.backend.model.database

import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.IdTable
import org.jetbrains.exposed.sql.Column
import org.jetbrains.exposed.sql.jodatime.datetime

object UserMeta : IdTable<Int>("wpv1_usermeta") {
    override val id: Column<EntityID<Int>> = integer("umeta_id").entityId()
    val userId = integer("user_id")
    val metaKey = varchar("meta_key", 255)
    val metaValue = text("meta_value")
}