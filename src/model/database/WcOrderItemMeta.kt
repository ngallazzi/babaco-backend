package com.babacomarket.backend.model.database

import org.jetbrains.exposed.dao.id.IdTable

object WcOrderItemMeta : IdTable<Int>("wpv1_woocommerce_order_itemmeta") {
    override val id = integer("meta_id").entityId()
    val orderItemId = integer("order_item_id")
    val metaKey = varchar("meta_key", 255)
    val metaValue = text("meta_value")
}