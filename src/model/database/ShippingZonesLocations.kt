package com.babacomarket.backend.model.database

import org.jetbrains.exposed.sql.Table

object ShippingZonesLocations :
    Table("wpv1_woocommerce_shipping_zone_locations") {
    val locationId = integer("location_id").uniqueIndex()
    val id = integer("zone_id") references ShippingZonesMethods.zoneId
    val code = varchar("location_code", 200)
    val locationType = varchar("location_type", 40)
}