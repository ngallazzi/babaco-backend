package com.babacomarket.backend.model.database

import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.IdTable
import org.jetbrains.exposed.sql.Column
import org.jetbrains.exposed.sql.Table
import org.jetbrains.exposed.sql.jodatime.datetime
import org.joda.time.DateTime


object Posts : IdTable<Int>("wpv1_posts") {
    override val id: Column<EntityID<Int>> = integer("ID").entityId()
    val author: Column<Int> = integer("post_author")
    val title = varchar("post_title", 50)
    val date: Column<DateTime> = datetime("post_date")
    val dateGMT: Column<DateTime> = datetime("post_date_gmt")
    val content = text("post_content")
    val excerpt = text("post_excerpt")
    val guid = text("guid")
    val postType = text("post_type")
    val toPing = text("to_ping")
    val pinged = text("pinged")
    val modified: Column<DateTime> = datetime("post_modified")
    val modifiedGMT: Column<DateTime> = datetime("post_modified_gmt")
    val postContentFiltered = text("post_content_filtered")
    val postMimeType = text("post_mime_type")
    val postStatus = text("post_status")
    val commentStatus = text("comment_status")
    val pingStatus = text("ping_status")
    val postPassword = text("post_password")
    val postName = text("post_name")
    val postParent = integer("post_parent")
}
