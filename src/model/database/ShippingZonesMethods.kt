package com.babacomarket.backend.model.database

import org.jetbrains.exposed.sql.Table

object ShippingZonesMethods :
    Table("wpv1_woocommerce_shipping_zone_methods") {
    val zoneId = integer("zone_id") references ShippingZones.id
    val instanceId = integer("instance_id").uniqueIndex()
    val methodId = varchar("method_id", 200)
    val methodOrder = integer("method_order")
    val isEnabled = bool("is_enabled")
}