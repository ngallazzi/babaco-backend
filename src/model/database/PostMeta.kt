package com.babacomarket.backend.model.database

import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.IdTable
import org.jetbrains.exposed.sql.Column

object PostMeta : IdTable<Int>("wpv1_postmeta") {
    override val id: Column<EntityID<Int>> = integer("meta_id").entityId()
    val postId = integer("post_id").references(Posts.id)
    val metaKey = varchar("meta_key", 50)
    val metaValue = text("meta_value")
}