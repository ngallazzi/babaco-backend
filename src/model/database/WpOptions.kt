package com.babacomarket.backend.model.database

import org.jetbrains.exposed.sql.Column
import org.jetbrains.exposed.sql.Table

object WpOptions : Table("wpv1_options") {
    val id: Column<Int> = integer("option_id").uniqueIndex()
    val name = varchar("option_name", 191)
    val value = text("option_value")

    fun toOptionName(id: String): String {
        val transformedId = id.replace(":", "_")
        return """woocommerce_${transformedId}_settings"""
    }

    fun toId(optionName: String): String {
        return optionName
            .replace("woocommerce_", "")
            .replace("_settings", "")
            .replace("_", ":")
            .replaceFirst(":", "_")
    }
}