package com.babacomarket.backend.model.database

import org.jetbrains.exposed.sql.Table
import org.jetbrains.exposed.sql.jodatime.datetime

object WcOrderCouponLookup :
    Table("wpv1_wc_order_coupon_lookup") {
    val orderId = integer("order_id")
    val couponId = integer("coupon_id")
    val dateCreated = datetime("date_created")
    val discountAmount = double("discount_amount")
}