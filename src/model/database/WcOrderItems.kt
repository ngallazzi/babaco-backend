package com.babacomarket.backend.model.database

import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.IdTable
import org.jetbrains.exposed.sql.Column

object WcOrderItems : IdTable<Int>("wpv1_woocommerce_order_items") {
    override val id : Column<EntityID<Int>> = integer("order_item_id").entityId()
    val name = text("order_item_name")
    val type = varchar("order_item_type", 200)
    val orderId = integer("order_id")
}