package com.babacomarket.backend.model.database

import org.jetbrains.exposed.sql.Table

object ShippingZones :
    Table("wpv1_woocommerce_shipping_zones") {
    val id = integer("zone_id").uniqueIndex()
    val name = varchar("zone_name", 200)
    val order = integer("zone_order")
}