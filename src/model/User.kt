package com.babacomarket.backend.model

import com.google.gson.annotations.SerializedName
import io.ktor.auth.*
import java.io.Serializable

data class User(
    val id: Int,
    @SerializedName("username")
    val userName: String,
    @Transient
    val passwordHash: String,
    val email: String,
    @SerializedName("registration_date")
    val registrationDate: String,
    @SerializedName("display_name")
    val displayName: String,
    @SerializedName("token")
    var token: String? = null
) : Serializable, Principal