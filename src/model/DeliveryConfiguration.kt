package com.babacomarket.backend.model

import java.time.DayOfWeek

class DeliveryConfiguration(val deliveryDay: DayOfWeek, val enabled: Boolean, val timeFrameId: String?)