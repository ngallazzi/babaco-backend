package com.babacomarket.backend.model

import java.time.DayOfWeek

data class DeliveryDay(val dayOfWeek: DayOfWeek, val timeFrames : List<TimeFrame>)