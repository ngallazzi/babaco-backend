package com.babacomarket.backend.model

import com.google.gson.annotations.SerializedName

class PaymentInfo(
    @SerializedName("stripe_customer_id")
    val stripeCustomerId: String,
    @SerializedName("stripe_source_id")
    val stripeSourceId: String,
    @SerializedName("payment_method")
    val paymentMethod: String = "stripe",
    @SerializedName("payment_method_title")
    val paymentMethodTitle: String = "credit_card",
) {
    companion object {
        fun getInstance(metas: List<Pair<String, String>>): PaymentInfo {
            return PaymentInfo(
                metas.find { it.first == "_stripe_customer_id" }!!.second,
                metas.find { it.first == "_stripe_source_id" }!!.second,
                metas.find { it.first == "_payment_method" }!!.second,
                metas.find { it.first == "_payment_method_title" }!!.second,
            )
        }
    }
}