package com.babacomarket.backend.model.shipping

import com.babacomarket.backend.model.Meta
import com.google.gson.annotations.SerializedName

open class Shipping(
    @SerializedName("first_name")
    val firstName: String,
    @SerializedName("last_name")
    val lastName: String,
    @SerializedName("address")
    val address: String,
    @SerializedName("city")
    val city: String,
    @SerializedName("state")
    val state: String,
    @SerializedName("post_code")
    val postCode: String,
    @SerializedName("country")
    val country: String = "IT"
) {

    companion object {
        fun getInstance(metas: List<Pair<String, String>>): Shipping {
            val firstName = metas.find { it.first == "_shipping_first_name" }?.second!!
            val lastName = metas.find { it.first == "_shipping_last_name" }?.second!!
            val address = metas.find { it.first == "_shipping_address_1" }?.second!!
            val city = metas.find { it.first == "_shipping_city" }?.second!!
            val state = metas.find { it.first == "_shipping_state" }?.second!!
            val postCode = metas.find { it.first == "_shipping_postcode" }?.second!!
            val country = metas.find { it.first == "_shipping_country" }?.second!!
            return Shipping(firstName, lastName, address, city, state, postCode, country)
        }
    }

     fun getMetas(): ArrayList<Meta> {
        val metas = arrayListOf<Meta>()
        metas.add(Meta("_shipping_address_1", "", address))
        metas.add(Meta("_shipping_address_2", "", ""))
        metas.add(Meta("_shipping_address_index", "", getShippingAddressIndex()))
        metas.add(Meta("_shipping_city", "", city))
        metas.add(Meta("_shipping_company", "", ""))
        metas.add(Meta("_shipping_country", "", country))
        metas.add(Meta("_shipping_first_name", "", firstName))
        metas.add(Meta("_shipping_last_name", "", lastName))
        metas.add(Meta("_shipping_postcode", "", postCode))
        metas.add(Meta("_shipping_state", "", state))
        return metas
    }

    private fun getShippingAddressIndex(): String {
        return "$firstName $lastName $address $city $state $postCode $country"
    }
}