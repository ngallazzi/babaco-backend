package com.babacomarket.backend.model.shipping

import com.google.gson.annotations.SerializedName

data class ShippingLines(
    @SerializedName("method_title")
    val methodTitle: String,
    @SerializedName("method_id")
    val methodId: String,
    @SerializedName("total")
    val total: String
)

