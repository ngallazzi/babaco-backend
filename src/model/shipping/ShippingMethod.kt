package com.babacomarket.backend.model.shipping

import com.babacomarket.backend.model.ZipCode

data class ShippingMethod(
    val id: String,
    var title: String?,
    var cost: Double?
)