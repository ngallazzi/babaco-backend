package com.babacomarket.backend.model

import com.babacomarket.backend.model.shipping.Shipping
import com.google.gson.annotations.SerializedName
import model.billing.Billing

data class Subscription(
    val id: Int,
    @SerializedName("date_created")
    val dateCreated: String,
    val total: Double,
    val billing: Billing,
    val shipping: Shipping,
    val frequency: Frequency,
    @SerializedName("next_payment")
    val nextPayment: String,
    val delivery: Delivery,
    val type: String,
    val status: String
){
    enum class Frequency(period: String, val billingInterval: Int) {
        WEEKLY("week", 1),
        EVERY_TWO_WEEKS("week", 2);
    }
}