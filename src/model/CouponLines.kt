package com.babacomarket.backend.model

import com.google.gson.annotations.SerializedName

data class CouponLines(
    @SerializedName("code")
    val code: String,
    @SerializedName("discount")
    val discount: String,
)

