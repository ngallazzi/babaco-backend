package com.babacomarket.backend.model

import com.google.gson.annotations.SerializedName

data class LineItem(
    @SerializedName("name")
    val name: String,
    @SerializedName("product_id")
    val productId: String,
    @SerializedName("variation_id")
    val variationId: String,
    @SerializedName("quantity")
    val quantity: String,
    @SerializedName("price")
    val price: String,
    @SerializedName("subtotal")
    val subtotal: String,
    @SerializedName("total")
    val total: String,
    @SerializedName("meta")
    val meta: List<Meta>
)

