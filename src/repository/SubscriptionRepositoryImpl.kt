package com.babacomarket.backend.repository

import com.babacomarket.backend.model.*
import com.babacomarket.backend.model.database.PostMeta
import com.babacomarket.backend.model.database.Posts
import com.babacomarket.backend.model.database.WcOrderItemMeta
import com.babacomarket.backend.model.database.WcOrderItems
import com.babacomarket.backend.model.shipping.Shipping
import com.babacomarket.backend.view.api.Errors
import com.babacomarket.backend.view.api.Result
import io.ktor.http.*
import kotlinx.coroutines.*
import model.billing.Billing
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.transactions.transaction
import org.joda.time.DateTime
import org.joda.time.DateTimeZone
import shared.toPostName
import shared.toPostTitle


class SubscriptionRepositoryImpl(
    private val deliveryRepository: DeliveryRepository,
    private val orderItemsRepository: OrdersRepository
) : SubscriptionRepository {

    override suspend fun getSubscriptions(userId: String): Result<List<Subscription>> = withContext(Dispatchers.IO) {
        val subscriptionsMap = transaction {
            val result = PostMeta
                .slice(PostMeta.postId)
                .select {
                    PostMeta.metaKey.eq("_customer_user")
                        .and(PostMeta.metaValue.eq(userId))
                }
            val postIds = result.map { it[PostMeta.postId] }

            val customerPostsResult = Posts
                .slice(Posts.id, Posts.postStatus)
                .select {
                    Posts.postType.eq("shop_subscription")
                        .and(Posts.postStatus.inList(listOf("wc-active", "wc-on-hold")))
                        .and(Posts.id.inList(postIds))
                }
            return@transaction customerPostsResult.map { it[Posts.id].value to it[Posts.postStatus] }
        }

        val subscriptions = arrayListOf<Subscription>()
        for (item in subscriptionsMap) {
            val subscription = getSubscriptionById(item.first, item.second)
            subscription?.run {
                subscriptions.add(subscription)
            }
        }
        return@withContext Result.Success(subscriptions)
    }

    private fun getSubscriptionById(id: Int, status: String?): Subscription? {
        try {
            val metas = getSubscriptionMeta(id)
            // Shipping
            val shipping = Shipping.getInstance(metas)
            // Billing
            val billing = Billing.getInstance(metas)

            // Delivery
            val deliveryTimeFrame = deliveryRepository.getDeliveryTimeFrame(metas) as Result.Success
            val deliveryDate = metas.find { it.first == "_delivery_date" }?.second!!
            val delivery = Delivery(deliveryDate, deliveryTimeFrame.data)

            // Frequency
            val frequency = getSubscriptionFrequency(billing.interval!!)!!

            val nextPayment = metas.find { it.first == "_schedule_next_payment" }?.second!!.replace(' ', 'T')
            val dateCreated = metas.find { it.first == "_schedule_start" }?.second!!.replace(' ', 'T')
            val total = metas.find { it.first == "_order_total" }?.second!!.toDouble()
            val type = getType(id)

            status?.let {
                return Subscription(
                    id, dateCreated, total, billing, shipping, frequency, nextPayment,
                    delivery,
                    type,
                    it
                )
            } ?: kotlin.run {
                return Subscription(
                    id, dateCreated, total, billing, shipping, frequency, nextPayment,
                    delivery,
                    type,
                    getSubscriptionStatus(id)
                )
            }

        } catch (e: Exception) {
            println(e)
            return null
        }
    }

    private fun getSubscriptionStatus(id: Int): String {
        val subscriptionPost = transaction {
            val postsResult = Posts
                .slice(Posts.id, Posts.postStatus)
                .select {
                    Posts.postType
                        .eq("shop_subscription")
                        .and(Posts.id.eq(id))
                }
            return@transaction postsResult.map { it[Posts.id].value to it[Posts.postStatus] }
        }
        return subscriptionPost.first().second
    }


    private fun getSubscriptionMeta(subscriptionId: Int): List<Pair<String, String>> {
        return transaction {
            val postMeta = PostMeta
                .slice(PostMeta.metaKey, PostMeta.metaValue)
                .select {
                    PostMeta.postId.eq(subscriptionId)
                }
            return@transaction postMeta.map { it[PostMeta.metaKey] to it[PostMeta.metaValue] }
        }
    }

    private fun getSubscriptionFrequency(interval: Int): Subscription.Frequency? {
        return when (interval) {
            1 -> Subscription.Frequency.WEEKLY
            2 -> Subscription.Frequency.EVERY_TWO_WEEKS
            else -> null
        }
    }

    override suspend fun createSubscription(
        subscriptionPostMeta: List<Meta>,
        subscriptionShippingItemMeta: List<Meta>,
        subscriptionProductItemMeta: List<Meta>,
        parentOrder: Order
    ): Result<Int> {
        try {
            val currentDate = DateTime().withZone(DateTimeZone.getDefault())
            val currentDateUtc = currentDate.withZone(DateTimeZone.UTC)

            val subscriptionId = transaction {
                return@transaction Posts.insert {
                    it[title] = "Subscription &ndash; " + currentDate.toPostTitle()
                    it[author] = 1
                    it[date] = currentDate
                    it[dateGMT] = currentDateUtc
                    it[modified] = currentDate
                    it[modifiedGMT] = currentDateUtc
                    it[postStatus] = "wc-active"
                    it[commentStatus] = "closed"
                    it[pingStatus] = "closed"
                    it[postParent] = parentOrder.id
                    it[postName] = "subscription-" + currentDate.toPostName()
                    it[postType] = "shop_subscription"
                    it[content] = ""
                    it[excerpt] = ""
                    it[pinged] = ""
                    it[toPing] = ""
                    it[postContentFiltered] = ""
                } get Posts.id
            }

            CoroutineScope(Dispatchers.IO + CoroutineName("postGuidUpdate")).launch {
                transaction {
                    val guid =
                        """${System.getenv("BABACO_URL")}?post_type=shop_subscription&#038;p=${subscriptionId.value}"""
                    Posts.update({ Posts.id eq subscriptionId }) {
                        it[Posts.guid] = guid
                    }
                }
            }

            CoroutineScope(Dispatchers.IO + CoroutineName("subscriptionPostMeta")).launch {
                transaction {
                    PostMeta.batchInsert(subscriptionPostMeta) { meta ->
                        this[PostMeta.postId] = subscriptionId.value
                        this[PostMeta.metaKey] = meta.key
                        this[PostMeta.metaValue] = meta.value
                    }
                }
            }

            CoroutineScope(Dispatchers.IO + CoroutineName("createOrderItem")).launch {
                transaction {
                    val shippingOrderItemId = WcOrderItems.insert {
                        it[name] = parentOrder.delivery.timeFrame.shippingMethods!!.first().title!!
                        it[type] = OrderItem.Type.SHIPPING.id
                        it[orderId] = subscriptionId.value
                    } get WcOrderItems.id

                    WcOrderItemMeta.batchInsert(subscriptionShippingItemMeta) { meta ->
                        this[WcOrderItemMeta.orderItemId] = shippingOrderItemId.value
                        this[WcOrderItemMeta.metaKey] = meta.key
                        this[WcOrderItemMeta.metaValue] = meta.value
                    }
                }
            }

            CoroutineScope(Dispatchers.IO + CoroutineName("createProductItem")).launch {
                transaction {
                    val productOrderItemId = WcOrderItems.insert {
                        it[name] = parentOrder.product.variations!!.first().name
                        it[type] = OrderItem.Type.LINE_ITEM.id
                        it[orderId] = subscriptionId.value
                    } get WcOrderItems.id

                    WcOrderItemMeta.batchInsert(subscriptionProductItemMeta) { meta ->
                        this[WcOrderItemMeta.orderItemId] = productOrderItemId.value
                        this[WcOrderItemMeta.metaKey] = meta.key
                        this[WcOrderItemMeta.metaValue] = meta.value
                    }
                }
            }
            println("returning subscription id")
            return when (subscriptionId.value) {
                -1 -> Result.Error(HttpStatusCode.InternalServerError, Exception(Errors.UNABLE_TO_CREATE_SUBSCRIPTION))
                else -> Result.Success(subscriptionId.value)
            }

        } catch (e: Exception) {
            return Result.Error(HttpStatusCode.InternalServerError, e)
        }
    }

    private fun getType(orderId: Int): String {
        val orderItems = orderItemsRepository.getOrderItems(orderId) as Result.Success
        return orderItems.data.find { it.type == OrderItem.Type.LINE_ITEM }!!.name
    }
}