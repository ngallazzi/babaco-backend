package com.babacomarket.backend.repository

import com.babacomarket.backend.model.User
import com.babacomarket.backend.view.api.Result

interface UsersRepository {

    suspend fun getUserById(userId: Int): Result<User>

    suspend fun findUser(username: String, password: String): Result<User>

    suspend fun userByUsername(username: String): Result<User?>

    suspend fun createUser(email: String, passwordHash: String) : Result<User>
}