package com.babacomarket.backend.repository

import com.babacomarket.backend.model.Meta
import com.babacomarket.backend.view.api.Result

interface UserMetaRepository {
    suspend fun getUserMetas(userId: Int): Result<List<Meta>>

    suspend fun getUserMeta(userId: Int, metaKey: String): Result<Meta>

    suspend fun updateUserMeta(userId: Int, metaKey: String, metaValue: String) : Result<Meta>
}