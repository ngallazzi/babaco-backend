package com.babacomarket.backend.repository

import com.babacomarket.backend.model.Product
import com.babacomarket.backend.model.database.PostMeta
import com.babacomarket.backend.model.database.Posts
import com.babacomarket.backend.view.api.Errors
import com.babacomarket.backend.view.api.Result
import io.ktor.http.*
import org.jetbrains.exposed.sql.and
import org.jetbrains.exposed.sql.select
import org.jetbrains.exposed.sql.transactions.transaction

class ProductRepositoryImpl : ProductRepository {
    private val products: ArrayList<Product> = getProductsFromDB()

    override fun getProducts(forceUpdate: Boolean): Result<ArrayList<Product>> {
        if (forceUpdate || products.isEmpty()) {
            products.clear()
            products.addAll(getProductsFromDB())
        }
        return Result.Success(products)
    }

    private fun getProductsFromDB(): ArrayList<Product> {
        val products = arrayListOf<Product>()
        transaction {
            val postsResult = Posts
                .slice(Posts.id, Posts.title, Posts.excerpt, Posts.content)
                .select {
                    Posts.postType
                        .eq("product")
                        .and(Posts.postStatus.eq("publish"))
                }
            for (productItem in postsResult) {
                try {
                    val product =
                        Product(
                            productItem[Posts.id].value,
                            productItem[Posts.title],
                            productItem[Posts.excerpt],
                            productItem[Posts.content]
                        )
                    product.price = getProductPrice(product.id)
                    product.images = getProductImages(product.id)
                    product.variations = getProductVariations(product.id)
                    products.add(product)
                } catch (e: Exception) {
                    print(e)
                }
            }
        }
        return products
    }

    override fun getProduct(variationId: Int): Result<Product> {
        products.find {
            it.variations!!.any { variation ->
                variation.id == variationId
            }
        }?.let {
            return Result.Success(it)
        } ?: kotlin.run {
            return Result.Error(HttpStatusCode.NotFound, Exception(Errors.PRODUCT_NOT_FOUND))
        }
    }

    override fun getVariation(variationId: Int): Result<Product.Variation> {
        val variations = products.flatMap { it.variations!! }
        variations.find { it.id == variationId }?.let {
            return Result.Success(it)
        } ?: kotlin.run {
            return Result.Error(HttpStatusCode.NotFound, Exception(Errors.PRODUCT_NOT_FOUND))
        }
    }

    private fun getProductPrice(id: Int): Double {
        val metaResult = PostMeta.select { PostMeta.postId.eq(id) }
        val priceRow = metaResult.first { it[PostMeta.metaKey] == "_subscription_price" }

        return priceRow[PostMeta.metaValue].toDouble()
    }

    private fun getProductImages(id: Int): List<Product.Image> {
        val imagesResult = Posts
            .slice(Posts.id, Posts.guid)
            .select {
                Posts.postParent
                    .eq(id)
                    .and(Posts.postType.eq("attachment"))
                    .and(Posts.postStatus.eq("inherit"))
                    .and(Posts.postMimeType.eq("image/jpeg"))
            }
        val images = arrayListOf<Product.Image>()
        for (item in imagesResult) {
            val image = Product.Image(item[Posts.id].value, item[Posts.guid])
            images.add(image)
        }
        return images
    }

    private fun getProductVariations(id: Int): List<Product.Variation> {
        val variationsResult = Posts
            .slice(Posts.id, Posts.title)
            .select {
                Posts.postParent
                    .eq(id)
                    .and(Posts.postType.eq("product_variation"))
                    .and(Posts.postParent.eq(id))
                    .and(Posts.postStatus.eq("publish"))
            }
        val variations = arrayListOf<Product.Variation>()
        for (item in variationsResult) {
            val variation = Product.Variation(item[Posts.id].value, item[Posts.title])
            variations.add(variation)
        }
        return variations
    }
}