package com.babacomarket.backend.repository

import com.babacomarket.backend.model.*
import com.babacomarket.backend.model.database.*
import com.babacomarket.backend.model.shipping.Shipping
import com.babacomarket.backend.view.api.Result
import io.ktor.http.*
import kotlinx.coroutines.*
import model.billing.Billing
import org.jetbrains.exposed.sql.batchInsert
import org.jetbrains.exposed.sql.insert
import org.jetbrains.exposed.sql.select
import org.jetbrains.exposed.sql.transactions.transaction
import org.jetbrains.exposed.sql.update
import org.joda.time.DateTime
import org.joda.time.DateTimeZone
import shared.toISO8601WithT
import shared.toPostName
import shared.toPostTitle

class OrdersRepositoryImpl : OrdersRepository {
    override fun getOrderItems(orderId: Int): Result<List<OrderItem>> {
        val orderItems = arrayListOf<OrderItem>()
        transaction {
            val result = WcOrderItems
                .select {
                    WcOrderItems.orderId.eq(orderId)
                }
            for (row in result) {
                val type = when (row[WcOrderItems.type]) {
                    OrderItem.Type.LINE_ITEM.id -> OrderItem.Type.LINE_ITEM
                    OrderItem.Type.COUPON.id -> OrderItem.Type.COUPON
                    OrderItem.Type.SHIPPING.id -> OrderItem.Type.SHIPPING
                    else -> null
                }
                orderItems.add(
                    OrderItem(
                        row[WcOrderItems.id].value,
                        row[WcOrderItems.name],
                        type,
                        row[WcOrderItems.orderId]
                    )
                )
            }
        }
        return Result.Success(orderItems)
    }

    override fun createOrder(
        customerNote: String,
        product: Product,
        variation: Product.Variation,
        userId: Int,
        orderMetas: List<Meta>,
        orderItemsMeta: List<Meta>
    ): Result<Int> {
        return try {
            val id = transaction {
                val currentDate = DateTime().withZone(DateTimeZone.getDefault())
                val currentDateUtc = currentDate.withZone(DateTimeZone.UTC)
                return@transaction Posts.insert {
                    it[author] = 1
                    it[date] = currentDate
                    it[dateGMT] = currentDateUtc
                    it[content] = ""
                    it[title] = "Order &ndash; " + currentDate.toPostTitle()
                    it[excerpt] = customerNote
                    it[postStatus] = "wc-completed"
                    it[commentStatus] = "closed"
                    it[pingStatus] = "closed"
                    // todo check if this field is mandatory
                    it[postPassword] = ""
                    it[postName] = "ordine-" + currentDate.toPostName()
                    it[toPing] = ""
                    it[pinged] = ""
                    it[modified] = currentDate
                    it[modifiedGMT] = currentDateUtc
                    it[postContentFiltered] = ""
                    it[postParent] = 0
                    it[postType] = "shop_order"
                    it[postMimeType] = ""
                } get Posts.id
            }

            runBlocking {
                launch(Dispatchers.IO + CoroutineName("guidUpdater")) {
                    updatePostsWithGuid(id.value)
                }

                val createOrderMetasJob =
                    launch(Dispatchers.IO + CoroutineName("createOrderMetas")) {
                        createOrderMetas(
                            id.value,
                            orderMetas,
                        )
                    }

                val createOrderItemsJob =
                    async(Dispatchers.IO + CoroutineName("createOrderItems")) {
                        createOrderItems(id.value, variation.name)
                    }

                val orderItemId = createOrderItemsJob.await()

                val createOrderItemMetasJob =
                    launch(Dispatchers.IO + CoroutineName("createOrderItemMetas")) {
                        createOrderItemMetas(orderItemId, orderItemsMeta)
                    }

                val createOrderProductLookupJob =
                    launch(Dispatchers.IO + CoroutineName("createOrderProductLookup")) {
                        createOrderProductLookup(orderItemId, id.value, product.id, variation.id, userId)
                    }

                createOrderItemsJob.join()
                createOrderItemMetasJob.join()
                createOrderProductLookupJob.join()
                createOrderMetasJob.join()
            }


            Result.Success(id.value)
        } catch (e: Exception) {
            Result.Error(HttpStatusCode.InternalServerError, e)
        }
    }

    override fun getOrder(
        id: Int,
        deliveryRepository: DeliveryRepository,
        productRepository: ProductRepository
    ): Result<Order> {
        return transaction {
            val postsResult = Posts.slice(Posts.id, Posts.date, Posts.excerpt, Posts.postStatus)
                .select {
                    Posts.id.eq(id)
                }
            val postsRow = postsResult.first()
            val actualId = postsRow[Posts.id].value
            val actualDate = postsRow[Posts.date]
            val customerNote = postsRow[Posts.excerpt]
            val status = postsRow[Posts.postStatus]

            val postMetaResult = transaction {
                PostMeta.slice(PostMeta.metaKey, PostMeta.metaValue)
                    .select {
                        PostMeta.postId.eq(id)
                    }
            }
            val metas = postMetaResult.map { it[PostMeta.metaKey] to it[PostMeta.metaValue] }
            val shipping = Shipping.getInstance(metas)
            val paymentInfo = PaymentInfo.getInstance(metas)

            val orderProductLookupResult = transaction {
                WcOrderProductLookup
                    .slice(WcOrderProductLookup.productId, WcOrderProductLookup.variationId)
                    .select {
                        WcOrderProductLookup.orderId.eq(id)
                    }
            }

            val orderProductLookupRow = orderProductLookupResult.first()
            val variationId = orderProductLookupRow[WcOrderProductLookup.variationId]
            val product = productRepository.getProduct(variationId) as Result.Success
            val variation = product.data.variations!!.first { it.id == variationId }

            val billing = Billing.getInstance(metas, variation.getBillingPeriod(), variation.getBillingInterval())

            val deliveryDateMeta = metas.find { it.first == "_delivery_date" }!!
            val deliveryTimeFrameMeta = metas.find { it.first == "_delivery_time_frame" }!!
            val delivery = deliveryRepository.getDeliveryFromMeta(
                deliveryDateMeta.second,
                deliveryTimeFrameMeta.second
            ) as Result.Success


            val shippingCost = metas.find { it.first == "_order_shipping" }!!.second.toDouble()
            val total = metas.find { it.first == "_order_total" }!!.second.toDouble()

            return@transaction Result.Success(
                Order(
                    actualId, actualDate.toISO8601WithT(), customerNote, status,
                    billing, delivery.data, product.data, variationId, shippingCost, total, shipping, paymentInfo
                )
            )
        }
    }

    private fun updatePostsWithGuid(orderId: Int) {
        transaction { // updates guid id
            val guid = """${System.getenv("BABACO_URL")}?post_type=shop_order&#038;p=$orderId"""
            Posts.update({ Posts.id eq orderId }) {
                it[Posts.guid] = guid
            }
        }
    }

    private fun createOrderMetas(
        orderId: Int,
        orderMetas: List<Meta>
    ) {
        transaction { // updates guid id
            PostMeta.batchInsert(orderMetas) { meta ->
                this[PostMeta.postId] = orderId
                this[PostMeta.metaKey] = meta.key
                this[PostMeta.metaValue] = meta.value
            }
        }
    }

    private fun createOrderProductLookup(
        orderItemId: Int,
        orderId: Int,
        productId: Int,
        variationId: Int,
        customerId: Int
    ): Int {
        val inserted = transaction { // updates guid id
            WcOrderProductLookup.insert {
                it[id] = orderItemId
                it[WcOrderProductLookup.orderId] = orderId
                it[WcOrderProductLookup.productId] = productId
                it[WcOrderProductLookup.variationId] = variationId
                it[WcOrderProductLookup.customerId] = customerId
                it[dateCreated] = DateTime.now()
                it[productQty] = 1
                it[productNetRevenue] = 0.0
                it[productGrossRevenue] = 0.0
                it[couponAmount] = 0.0
                it[taxAmount] = 0.0
                it[shippingAmount] = 0.0
                it[shippingTaxAmount] = 0.0
            } get WcOrderProductLookup.id
        }
        return inserted.value
    }

    private fun createOrderItems(orderId: Int, subscriptionName: String): Int {
        return transaction {
            WcOrderItems.insert {
                it[name] = subscriptionName
                it[type] = "line_item"
                it[WcOrderItems.orderId] = orderId
            } get WcOrderItems.id
        }.value
    }


    private fun createOrderItemMetas(orderItemId: Int, metas: List<Meta>) {
        transaction {
            WcOrderItemMeta.batchInsert(metas) { meta ->
                this[WcOrderItemMeta.orderItemId] = orderItemId
                this[WcOrderItemMeta.metaKey] = meta.key
                this[WcOrderItemMeta.metaValue] = meta.value
            }
        }
    }
}