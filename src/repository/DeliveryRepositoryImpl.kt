package com.babacomarket.backend.repository

import com.babacomarket.backend.model.*
import com.babacomarket.backend.model.database.*
import com.babacomarket.backend.model.shipping.ShippingMethod
import com.babacomarket.backend.view.api.Errors
import com.babacomarket.backend.view.api.Result
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import io.ktor.http.*
import org.jetbrains.exposed.sql.and
import org.jetbrains.exposed.sql.select
import org.jetbrains.exposed.sql.selectAll
import org.jetbrains.exposed.sql.transactions.transaction
import org.joda.time.DateTime
import shared.toBoolean
import shared.unserializePHPToJson
import shared.unserializeToMap
import java.time.DayOfWeek

class DeliveryRepositoryImpl : DeliveryRepository {
    private val deliveryDaysOptions: ArrayList<DeliveryDay>
        get() = transaction {
            val optionsResult = WpOptions.select { WpOptions.name.eq("wc_od_delivery_days") }
            val row = optionsResult.first()
            return@transaction getDeliveryDaysFromSerializedPHP(row[WpOptions.value])
        }

    override fun getDeliveryDays(zipCode: String): Result<ArrayList<DeliveryDay>> {
        val zipCodeMethods = getShippingMethodsByZipCode(zipCode)
        val allowedDays = arrayListOf<DeliveryDay>()
        for (deliveryDay in deliveryDaysOptions) {
            val dayShippingMethods = deliveryDay.timeFrames.flatMap { it.shippingMethods!! }
            if (dayShippingMethods.intersect(zipCodeMethods).isNotEmpty()) {
                allowedDays.add(deliveryDay)
            }
        }
        return Result.Success(allowedDays)
    }

    private fun getDeliveryDaysFromSerializedPHP(input: String): ArrayList<DeliveryDay> {
        val deliveryDays = ArrayList<DeliveryDay>()
        val map = input.unserializeToMap()
        for (key in map.keys) {
            val dayIndex = key.toString().toInt()
            val dayMap = map[key] as Map<*, *>
            getDeliveryDay(dayMap, dayIndex)?.run {
                deliveryDays.add(this)
            }
        }
        return deliveryDays
    }

    private fun getDeliveryDay(dayMap: Map<*, *>, dayIndex: Int): DeliveryDay? {
        val timeFrames = arrayListOf<TimeFrame>()
        if (dayMap["enabled"] == "yes") {
            val timeFramesMap = dayMap["time_frames"] as Map<*, *>
            for (timeFrameElement in timeFramesMap) {
                val elementValue = timeFrameElement.value as Map<*, *>
                val title = elementValue["title"].toString()
                val timeFrom = elementValue["time_from"].toString()
                val timeTo = elementValue["time_to"].toString()
                val shippingMethodsMap = elementValue["shipping_methods"] as Map<*, *>
                val shippingMethods = arrayListOf<ShippingMethod>()
                // we need to retrieve shipping cost and title, woocommerce sucks
                for (shippingElement in shippingMethodsMap) {
                    findShippingMethod(shippingElement.value.toString())?.let {
                        shippingMethods.add(it)
                    }
                }

                if (shippingMethods.isNotEmpty()) {
                    timeFrames.add(
                        TimeFrame(
                            "time_frame:" + timeFrameElement.key,
                            title,
                            timeFrom,
                            timeTo,
                            shippingMethods
                        ),
                    )
                }
            }
        }
        return when (dayIndex) {
            0 -> DeliveryDay(DayOfWeek.SUNDAY, timeFrames)
            1 -> DeliveryDay(DayOfWeek.MONDAY, timeFrames)
            2 -> DeliveryDay(DayOfWeek.TUESDAY, timeFrames)
            3 -> DeliveryDay(DayOfWeek.WEDNESDAY, timeFrames)
            4 -> DeliveryDay(DayOfWeek.THURSDAY, timeFrames)
            5 -> DeliveryDay(DayOfWeek.FRIDAY, timeFrames)
            6 -> DeliveryDay(DayOfWeek.SATURDAY, timeFrames)
            else -> null
        }
    }

    private fun findShippingMethod(id: String): ShippingMethod? {
        try {
            val option = transaction {
                val optionsResult = WpOptions.select { WpOptions.name.eq(WpOptions.toOptionName(id)) }
                val row = optionsResult.first()
                return@transaction row[WpOptions.value].unserializeToMap()
            }
            val title = option["title"].toString()
            val cost = option["cost"].toString().replace(",", ".").toDouble()
            return ShippingMethod(id, title, cost)
        } catch (e: Exception) {
            print(e)
            return null
        }
    }

    private fun getShippingMethodsByZipCode(zipCode: String): List<ShippingMethod> {
        val shippingMethods = arrayListOf<ShippingMethod>()
        val methodsResult = transaction {
            val result = ShippingZonesLocations
                .innerJoin(ShippingZonesMethods)
                .innerJoin(ShippingZones)
                .slice(
                    ShippingZonesMethods.instanceId,
                    ShippingZonesMethods.methodId,
                    ShippingZonesLocations.code,
                    ShippingZones.id
                )
                .select {
                    ShippingZonesLocations.code
                        .eq(zipCode)
                        .and(ShippingZonesMethods.isEnabled.eq(true))
                }

            return@transaction result.map {
                WpOptions.toOptionName(
                    it[ShippingZonesMethods.methodId]
                            + ":" + it[ShippingZonesMethods.instanceId]
                ) to it[ShippingZonesLocations.code]
            }
        }

        val options = transaction {
            val optionsResult = WpOptions.select { WpOptions.name.inList(methodsResult.map { it.first }) }
            return@transaction optionsResult.map { it[WpOptions.name] to it[WpOptions.value].unserializeToMap() }
        }

        for (option in options) {
            val id = WpOptions.toId(option.first)
            val title = option.second["title"].toString()
            val cost = option.second["cost"].toString().replace(",", ".")
            val shippingMethod = ShippingMethod(id, title = title, cost.toDouble())
            shippingMethods.add(shippingMethod)
        }
        return shippingMethods
    }

    override fun getZipCodes(): Result<ArrayList<ZipCode>> {
        val codes = ArrayList<ZipCode>()
        transaction {
            val result = ZipCodes.selectAll()
            for (code in result) {
                codes.add(
                    ZipCode(
                        code[ZipCodes.id],
                        code[ZipCodes.code],
                        code[ZipCodes.city]
                    )
                )
            }
        }
        return Result.Success(codes)
    }

    override fun getDeliveryTimeFrame(metas: List<Pair<String, String>>): Result<TimeFrame> {
        val customerDeliveryDayMeta = metas.find { it.first == "_delivery_days" }?.second!!
        val json = customerDeliveryDayMeta.unserializePHPToJson()
        val customerMap = JsonParser.parseString(json).asJsonObject
        val customerPreferences = getCustomerPreferences(customerMap)
        val result = getCustomerTimeFrame(deliveryDaysOptions, customerPreferences)
        if (result != null) {
            return Result.Success(result)
        }
        return Result.Error(HttpStatusCode.NotFound, Exception(Errors.DELIVERY_TIME_FRAME_NOT_FOUND))
    }

    override fun getDeliveryTimeFrame(date: DateTime, key: String): Result<TimeFrame> {
        val dayOfTheWeek = date.dayOfWeek().get()
        deliveryDaysOptions.find { it.dayOfWeek.value == dayOfTheWeek }?.timeFrames?.find { it.key == key }?.apply {
            return Result.Success(this)
        }
        return Result.Error(HttpStatusCode.NotFound, Exception(Errors.DELIVERY_TIME_FRAME_NOT_FOUND))
    }

    override fun getDeliveryFromMeta(deliveryDateMeta: String, deliveryDateMetaTimeFrame: String): Result<Delivery> {
        val dayOfTheWeek = DateTime.parse(deliveryDateMeta).dayOfWeek().get()
        val dayTimeFrames = deliveryDaysOptions.find { it.dayOfWeek.value == dayOfTheWeek }?.timeFrames!!
        val serializedMeta = deliveryDateMetaTimeFrame.unserializeToMap()
        val searchedTimeFromMap = serializedMeta.filter { it.key == "time_from" }
        val searchedTimeToMap = serializedMeta.filter { it.key == "time_to" }
        val searchedTimeFrom = searchedTimeFromMap.values.first() as String
        val searchedTimeTo = searchedTimeToMap.values.first() as String
        val timeFrame = dayTimeFrames.find { it.timeFrom == searchedTimeFrom && it.timeTo == searchedTimeTo }!!
        return Result.Success(Delivery(deliveryDateMeta, timeFrame))
    }


    override fun getShippingCost(shippingMethodId: String): Result<Double> {
        val options = transaction {
            val optionsResult = WpOptions.select { WpOptions.name.eq(WpOptions.toOptionName(shippingMethodId)) }
            return@transaction optionsResult.map { it[WpOptions.name] to it[WpOptions.value].unserializeToMap() }
        }
        val cost = options.first().second["cost"].toString().replace(",", ".")
        return Result.Success(cost.toDouble())
    }

    private fun getCustomerPreferences(customerMap: JsonObject): ArrayList<DeliveryConfiguration> {
        val customerPreferences = arrayListOf<DeliveryConfiguration>()
        for (element in customerMap.entrySet()) {
            val enabled = element.value.asJsonObject.get("enabled")
            val timeframe = element.value.asJsonObject.get("time_frame")
            when (element.key) {
                "0" -> customerPreferences.add(
                    DeliveryConfiguration(
                        DayOfWeek.SUNDAY,
                        enabled.toBoolean(),
                        timeframe.asString
                    )
                )
                "1" -> customerPreferences.add(
                    DeliveryConfiguration(
                        DayOfWeek.MONDAY,
                        enabled.toBoolean(),
                        timeframe.asString
                    )
                )
                "2" -> customerPreferences.add(
                    DeliveryConfiguration(
                        DayOfWeek.TUESDAY,
                        enabled.toBoolean(),
                        timeframe.asString
                    )
                )
                "3" -> customerPreferences.add(
                    DeliveryConfiguration(
                        DayOfWeek.WEDNESDAY,
                        enabled.toBoolean(),
                        timeframe.asString
                    )
                )
                "4" -> customerPreferences.add(
                    DeliveryConfiguration(
                        DayOfWeek.THURSDAY,
                        enabled.toBoolean(),
                        timeframe.asString
                    )
                )
                "5" -> customerPreferences.add(
                    DeliveryConfiguration(
                        DayOfWeek.FRIDAY,
                        enabled.toBoolean(),
                        timeframe.asString
                    )
                )
                "6" -> customerPreferences.add(
                    DeliveryConfiguration(
                        DayOfWeek.SATURDAY,
                        enabled.toBoolean(),
                        timeframe.asString
                    )
                )
            }
        }
        return customerPreferences
    }

    private fun getCustomerTimeFrame(
        deliveryDays: ArrayList<DeliveryDay>,
        customerPreferences: ArrayList<DeliveryConfiguration>
    ): TimeFrame? {
        val preferredDay = customerPreferences.find { it.enabled && it.timeFrameId != null }
        preferredDay?.let { day ->
            val deliveryDay = deliveryDays.find { it.dayOfWeek == day.deliveryDay }
            return deliveryDay!!.timeFrames.first()
        }
        return null
    }
}