package com.babacomarket.backend.repository

import com.babacomarket.backend.model.Faq
import com.babacomarket.backend.model.database.Posts
import com.babacomarket.backend.view.api.Result
import org.jetbrains.exposed.sql.and
import org.jetbrains.exposed.sql.select
import org.jetbrains.exposed.sql.transactions.transaction

class PostsRepositoryImpl : PostsRepository {
    override fun getFaq(): Result<ArrayList<Faq>> {
        val faq = ArrayList<Faq>()
        transaction {
            val postsResult = Posts.select { Posts.title.eq("FAQ").and(Posts.postStatus.eq("publish")) }
            try {
                val row = postsResult.first()
                faq.addAll(getFaqs(row[Posts.content]))
            } catch (e: Exception) {

            }
        }
        return Result.Success(faq)
    }

    private fun getFaqs(postContent: String): ArrayList<Faq> {
        val faq = arrayListOf<Faq>()
        try {
            val toggleElements = postContent.split(FAQ_ELEMENT_PLACEHOLDER)
            for (element in toggleElements) {
                val titleStartIndex = element.indexOf(FAQ_TITLE_PLACEHOLDER_START)
                val titleEndIndex = element.lastIndexOf(FAQ_TITLE_PLACEHOLDER_END)
                val title = element.subSequence(titleStartIndex, titleEndIndex).toString()
                    .replace(FAQ_TITLE_PLACEHOLDER_START, "")
                    .replace(FAQ_TITLE_PLACEHOLDER_END, "")
                val content = element.subSequence(titleEndIndex, element.lastIndex).toString()
                    .replace(FAQ_TITLE_PLACEHOLDER_END, "")
                val faqEl = Faq(title, content)
                faq.add(faqEl)
            }
        } catch (e: Exception) {
            print(e)
        }
        return faq
    }

    companion object {
        const val FAQ_ELEMENT_PLACEHOLDER = "[/vc_toggle]"
        const val FAQ_TITLE_PLACEHOLDER_START = "[vc_toggle title=\""
        const val FAQ_TITLE_PLACEHOLDER_END = "\"]"
    }
}