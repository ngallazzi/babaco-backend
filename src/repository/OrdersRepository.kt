package com.babacomarket.backend.repository

import com.babacomarket.backend.model.*
import com.babacomarket.backend.view.api.Result

interface OrdersRepository {
    fun getOrderItems(orderId: Int): Result<List<OrderItem>>

    fun createOrder(
        customerNote: String,
        product: Product,
        variation: Product.Variation,
        userId: Int,
        orderMetas: List<Meta>,
        orderItemsMeta: List<Meta>
    ): Result<Int>

    fun getOrder(
        id: Int,
        deliveryRepository: DeliveryRepository,
        productRepository: ProductRepository
    ): Result<Order>
}