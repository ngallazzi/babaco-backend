package com.babacomarket.backend.repository

import com.babacomarket.backend.model.User
import com.babacomarket.backend.model.database.Users
import com.babacomarket.backend.view.api.Errors
import io.ktor.http.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.jetbrains.exposed.sql.insert
import org.jetbrains.exposed.sql.select
import org.jetbrains.exposed.sql.transactions.transaction
import org.joda.time.DateTime
import shared.PHPass
import shared.toISO8601WithT
import com.babacomarket.backend.view.api.Result as Result


class UsersRepositoryImpl : UsersRepository {
    override suspend fun getUserById(userId: Int): Result<User> = withContext(Dispatchers.IO) {
        val user = transaction {
            val result = Users.select { Users.id.eq(userId) }
            return@transaction result
                .map {
                    User(
                        it[Users.id].value,
                        it[Users.username],
                        it[Users.passwordHash],
                        it[Users.email],
                        it[Users.registrationDate].toISO8601WithT(),
                        it[Users.displayName]
                    )
                }.singleOrNull()
        }

        return@withContext if (user != null) {
            Result.Success(user)
        } else {
            Result.Error(HttpStatusCode.NotFound, Exception(Errors.USER_NOT_FOUND))
        }
    }

    override suspend fun userByUsername(username: String): Result<User> = withContext(Dispatchers.IO) {
        val user = transaction {
            val result = Users.select { Users.username.eq(username) }
            return@transaction result
                .map {
                    User(
                        it[Users.id].value,
                        it[Users.username],
                        it[Users.passwordHash],
                        it[Users.email],
                        it[Users.registrationDate].toISO8601WithT(),
                        it[Users.displayName]
                    )
                }.singleOrNull()
        }

        return@withContext if (user != null) {
            Result.Success(user)
        } else {
            Result.Error(HttpStatusCode.NotFound, Exception(Errors.USER_NOT_FOUND))
        }
    }

    override suspend fun createUser(email: String, passwordHash: String): Result<User> {
        transaction {
            return@transaction Users.insert {
                it[username] = email
                it[Users.passwordHash] = passwordHash
                it[Users.email] = email
                it[registrationDate] = DateTime.now()
            }
        }
        return userByUsername(email)
    }

    override suspend fun findUser(username: String, password: String): Result<User> = withContext(Dispatchers.IO) {
        return@withContext try {
            val result = userByUsername(username)
            if (result is Result.Success && passwordMatch(password, result.data.passwordHash)) {
                Result.Success(result.data)
            } else {
                Result.Error(HttpStatusCode.NotFound, Exception(Errors.USER_NOT_FOUND))
            }
        } catch (e: Exception) {
            Result.Error(HttpStatusCode.InternalServerError, e)
        }
    }

    private fun passwordMatch(password: String, storedHash: String): Boolean {
        val checker = PHPass(8)
        return checker.checkPassword(password, storedHash)
    }
}