package com.babacomarket.backend.repository

import com.babacomarket.backend.model.Meta
import com.babacomarket.backend.model.database.UserMeta
import com.babacomarket.backend.view.api.Errors
import com.babacomarket.backend.view.api.Result
import io.ktor.http.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.jetbrains.exposed.sql.and
import org.jetbrains.exposed.sql.select
import org.jetbrains.exposed.sql.transactions.transaction
import org.jetbrains.exposed.sql.update

class UserMetaRepositoryImpl : UserMetaRepository {
    override suspend fun getUserMetas(userId: Int): Result<List<Meta>> = withContext(Dispatchers.IO) {
        val metas = transaction {
            val result = UserMeta.select { UserMeta.userId.eq(userId) }
            return@transaction result
                .map {
                    Meta(it[UserMeta.metaKey], "", it[UserMeta.metaValue])
                }
        }

        return@withContext if (metas.isNotEmpty()) {
            Result.Success(metas)
        } else {
            Result.Error(HttpStatusCode.NotFound, Exception(Errors.USER_NOT_FOUND))
        }
    }

    override suspend fun getUserMeta(userId: Int, metaKey: String): Result<Meta> = withContext(Dispatchers.IO) {
        val meta = transaction {
            val result = UserMeta.select { UserMeta.userId.eq(userId).and(UserMeta.metaKey.eq(metaKey)) }
            return@transaction result
                .map {
                    Meta(it[UserMeta.metaKey], null, it[UserMeta.metaValue])
                }.firstOrNull()
        }

        meta?.let {
            return@withContext Result.Success(meta)
        } ?: kotlin.run {
            return@withContext Result.Error(HttpStatusCode.NotFound, Exception(Errors.USER_NOT_FOUND))
        }
    }

    override suspend fun updateUserMeta(userId: Int, metaKey: String, metaValue: String): Result<Meta> =
        withContext(Dispatchers.IO) {
            val updated = transaction {
                return@transaction UserMeta.update({ UserMeta.userId.eq(userId).and(UserMeta.metaKey.eq(metaKey)) }) {
                    it[UserMeta.metaValue] = metaValue
                }
            }
            return@withContext if (updated > 0) {
                Result.Success(Meta(metaKey, null, metaValue))
            } else {
                Result.Error(HttpStatusCode.NotFound, Exception(Errors.USER_NOT_FOUND))
            }
        }
}
