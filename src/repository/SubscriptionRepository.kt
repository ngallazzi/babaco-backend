package com.babacomarket.backend.repository

import com.babacomarket.backend.model.Meta
import com.babacomarket.backend.model.Order
import com.babacomarket.backend.model.Subscription
import com.babacomarket.backend.model.User
import com.babacomarket.backend.view.api.Result
import com.babacomarket.backend.view.api.checkout.requests.SubscriptionRequest

interface SubscriptionRepository {
    suspend fun getSubscriptions(userId: String): Result<List<Subscription>>

    suspend fun createSubscription(
        subscriptionPostMeta: List<Meta>,
        subscriptionShippingItemMeta: List<Meta>,
        subscriptionProductItemMeta: List<Meta>,
        parentOrder: Order
    ): Result<Int>
}