package com.babacomarket.backend.repository

import com.babacomarket.backend.model.*
import com.babacomarket.backend.view.api.Result
import org.joda.time.DateTime

interface DeliveryRepository {
    fun getDeliveryDays(zipCode: String): Result<ArrayList<DeliveryDay>>

    fun getZipCodes(): Result<ArrayList<ZipCode>>

    fun getDeliveryTimeFrame(metas: List<Pair<String, String>>): Result<TimeFrame>

    fun getDeliveryTimeFrame(date: DateTime, key: String): Result<TimeFrame>

    fun getDeliveryFromMeta(deliveryDateMeta: String,
                            deliveryDateMetaTimeFrame: String) : Result<Delivery>

    fun getShippingCost(shippingMethodId : String) : Result<Double>
}