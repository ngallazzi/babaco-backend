package com.babacomarket.backend.repository

import com.babacomarket.backend.model.Product
import com.babacomarket.backend.view.api.Result

interface ProductRepository {
    fun getProducts(forceUpdate: Boolean = false): Result<ArrayList<Product>>

    fun getProduct(variationId: Int): Result<Product>

    fun getVariation(variationId: Int): Result<Product.Variation>
}