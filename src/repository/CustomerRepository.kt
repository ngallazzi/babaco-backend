package com.babacomarket.backend.repository

import com.babacomarket.backend.view.api.Result
import com.babacomarket.backend.view.api.checkout.requests.CardInfo
import com.stripe.model.Customer

interface CustomerRepository {
    fun customerById(id: String): Result<Customer>

    fun createCardSource(customer: Customer, cardInfo: CardInfo): Result<Customer>
}