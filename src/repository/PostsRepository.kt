package com.babacomarket.backend.repository

import com.babacomarket.backend.model.Faq
import com.babacomarket.backend.view.api.Result

interface PostsRepository{
    fun getFaq() : Result<ArrayList<Faq>>
}