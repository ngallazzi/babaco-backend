package com.babacomarket.backend.repository

import com.babacomarket.backend.view.api.Result
import com.babacomarket.backend.view.api.checkout.requests.CardInfo
import com.stripe.model.Card
import com.stripe.model.Customer
import com.stripe.model.SetupIntent
import com.stripe.model.Token
import java.util.*

class CustomerRepositoryImpl : CustomerRepository {
    override fun customerById(id: String): Result<Customer> {
        val retrieveParams = mutableMapOf<String, Any>()
        val expandList: MutableList<String> = ArrayList()
        expandList.add("sources")
        retrieveParams["expand"] = expandList
        return Result.Success(Customer.retrieve(
            id,
            retrieveParams,
            null
        ))
    }

    override fun createCardSource(customer: Customer, cardInfo: CardInfo): Result<Customer> {
        val token = createCardToken(cardInfo)
        val card = createCardSource(customer.id, token!!)
        setupIntents(card)
        return customerById(customer.id)
    }

    private fun createCardToken(
        cardInfo: CardInfo
    ): Token? {
        val card = mutableMapOf<String, Any>()
        card["number"] = cardInfo.number
        card["exp_month"] = cardInfo.expMonth
        card["exp_year"] = cardInfo.expYear
        card["cvc"] = cardInfo.cvc
        card["name"] = cardInfo.name
        val params = mutableMapOf<String, Any>()
        params["card"] = card
        return Token.create(params)
    }

    private fun createCardSource(customerId: String, token: Token): Card {
        val retrieveParams = mutableMapOf<String, Any>()
        val expandList: MutableList<String> = ArrayList()
        expandList.add("sources")
        retrieveParams["expand"] = expandList
        val customer = Customer.retrieve(
            customerId,
            retrieveParams,
            null
        )

        val params: MutableMap<String, Any> = HashMap()
        params["source"] = token.id
        return customer.sources.create(params) as Card
    }

    private fun setupIntents(card: Card) {
        val params: MutableMap<String, Any> = HashMap()
        params["payment_method"] = card.id
        params["customer"] = card.customer
        params["confirm"] = true
        SetupIntent.create(params)
    }
}