package com.babacomarket.backend

import com.auth0.jwt.JWT
import com.auth0.jwt.JWTVerifier
import com.babacomarket.backend.model.User
import com.babacomarket.backend.view.api.Result
import com.babacomarket.backend.view.api.account.account
import com.babacomarket.backend.view.api.auth.auth
import com.babacomarket.backend.view.api.checkout.checkout
import com.babacomarket.backend.view.api.delivery.delivery
import com.babacomarket.backend.view.api.posts.posts
import com.babacomarket.backend.view.api.products.products
import com.babacomarket.backend.view.webapp.home
import freemarker.cache.ClassTemplateLoader
import io.ktor.application.*
import io.ktor.auth.*
import io.ktor.auth.jwt.*
import io.ktor.features.*
import io.ktor.freemarker.*
import io.ktor.gson.*
import io.ktor.http.*
import io.ktor.response.*
import io.ktor.routing.*
import io.ktor.util.*
import java.lang.Exception
import java.text.DateFormat

fun main(args: Array<String>): Unit = io.ktor.server.netty.EngineMain.main(args)

@KtorExperimentalAPI
@Suppress("unused") // Referenced in application.conf
@kotlin.jvm.JvmOverloads
fun Application.module(testing: Boolean = false) {
    initDB()

    install(Authentication) {
        jwt(AUTH_CONFIG_NAME) {
            realm = jwtRealm
            verifier(makeJwtVerifier())
            validate {
                val payload = it.payload
                val userId = payload.getClaim(jwtClaim).asInt()
                when (val user = accountController.getUserById(userId)) {
                    is Result.Success -> user.data!!
                    else -> return@validate null
                }
            }
        }
    }

    install(DefaultHeaders)

    install(StatusPages) {
        exception<Throwable> { e ->
            call.respond(
                HttpStatusCode.InternalServerError, e
            )
        }
    }

    install(ContentNegotiation) {
        gson {
            setDateFormat(DateFormat.LONG)
            setPrettyPrinting()
        }
    }

    install(FreeMarker) {
        templateLoader = ClassTemplateLoader(this::class.java.classLoader, "templates")
    }

    routing {
        this.home()
        this.auth(authController, jwtIssuer, jwtClaim, algorithm, *jwtAudience)
        this.account(accountController)
        this.delivery(deliveryController)
        this.checkout(checkoutController)
        this.products(productsController)
        this.posts(postsController)
    }
}

private fun makeJwtVerifier(): JWTVerifier = JWT
    .require(algorithm)
    .withAudience(*jwtAudience)
    .withIssuer(jwtIssuer)
    .build()

val ApplicationCall.user get() = authentication.principal<User>()

const val API_VERSION = "/api/v1"
const val AUTH_CONFIG_NAME = "jwt"



